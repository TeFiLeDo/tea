#include "db_checker.hpp"

#include <tntdb/connection.h>

extern tntdb::Connection dbCon;

namespace tea
{
	namespace helpers
	{
		unsigned db_checker::operator()(tnt::HttpRequest& request,
		                                tnt::HttpReply& reply,
		                                tnt::QueryParams& qparam)
		{
			if(dbCon.ping())
				return DECLINED;
			else
				return controllers::send_status(reply,
				                                HTTP_SERVICE_UNAVAILABLE);
		}
	} // namespace helpers
} // namespace tea
