#include "access_controll.hpp"

#include "../models/user.hpp"

namespace tea
{
	namespace helpers
	{
		access_controll::access_information::access_information(
		    access_controll::required_rights rights,
		    access_controll::method_types type)
		    : type(type), rights(rights)
		{}

		unsigned access_controll::operator()(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			static bool first_run = true;
			if(first_run)
			{
				// block rerun of this
				first_run = false;

				// fill redirs
				redirs["/"] = "/default/index";
				redirs["/default"] = "/default/index";
				redirs["/static/semantic/semantic.css"]
				    = "/static/semantic-ui-css/semantic.css";
				redirs["/static/semantic/semantic.js"]
				    = "/static/semantic-ui-css/semantic.js";
				redirs["/static/datatables/dataTables.semanticui.min.css"]
				    = "/static/DataTables/media/css/"
				      "dataTables.semanticui.min.css";
				redirs["/static/datatables/dataTables.semanticui.min.js"]
				    = "/static/DataTables/media/js/"
				      "dataTables.semanticui.min.js";
				redirs["/static/datatables/jquery.dataTables.js"]
				    = "/static/DataTables/media/js/jquery.dataTables.js";
				redirs["/static/jquery/jquery.min.js"]
				    = "/static/jquery-dist/dist/jquery.min.js";

				// fill paths
				//----------

				// default
				paths["/default/about"] = ai(R_NONE, M_GET);
				paths["/default/event-details"] = ai(R_NONE, M_GET);
				paths["/default/event-list"] = ai(R_USER, M_GET);
				paths["/default/event-list-data"] = ai(R_USER, M_GET);
				paths["/default/index"] = ai(R_NONE, M_GET);
				paths["/default/index-list-data"] = ai(R_NONE, M_GET);
				paths["/default/imprint"] = ai(R_NONE, M_GET);
				paths["/default/settings"] = ai(R_USER, M_GET);
				paths["/default/settings-password"] = ai(R_USER, M_POST);
				paths["/default/settings-store-session"] = ai(R_USER, M_POST);
				paths["/default/settings-username"] = ai(R_USER, M_POST);

				// event
				paths["/event/add"] = ai(R_ADMIN, M_BOTH);
				paths["/event/details"] = ai(R_ADMIN, M_GET);
				paths["/event/details-groups"] = ai(R_ADMIN, M_GET);
				paths["/event/details-users"] = ai(R_ADMIN, M_GET);
				paths["/event/list"] = ai(R_ADMIN, M_GET);
				paths["/event/list-data"] = ai(R_ADMIN, M_GET);
				paths["/event/remove"] = ai(R_ADMIN, M_POST);
				paths["/event/update"] = ai(R_ADMIN, M_GET);
				paths["/event/update-detail-info"] = ai(R_ADMIN, M_POST);
				paths["/event/update-general"] = ai(R_ADMIN, M_POST);
				paths["/event/update-group-add"] = ai(R_ADMIN, M_BOTH);
				paths["/event/update-group-remove"] = ai(R_ADMIN, M_POST);
				paths["/event/update-user-add"] = ai(R_ADMIN, M_BOTH);
				paths["/event/update-user-remove"] = ai(R_ADMIN, M_POST);

				// group
				paths["/group/add"] = ai(R_ADMIN, M_BOTH);
				paths["/group/details"] = ai(R_ADMIN, M_GET);
				paths["/group/details-user-list"] = ai(R_ADMIN, M_GET);
				paths["/group/list"] = ai(R_ADMIN, M_GET);
				paths["/group/list-data"] = ai(R_ADMIN, M_GET);
				paths["/group/remove"] = ai(R_ADMIN, M_POST);
				paths["/group/update"] = ai(R_ADMIN, M_GET);
				paths["/group/update-general"] = ai(R_ADMIN, M_POST);
				paths["/group/update-user-add"] = ai(R_ADMIN, M_BOTH);
				paths["/group/update-user-remove"] = ai(R_ADMIN, M_POST);

				// static - datatables
				paths
				    ["/static/DataTables/media/css/"
				     "dataTables.semanticui.min.css"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/DataTables/media/js/"
				     "dataTables.semanticui.min.js"]
				    = ai(R_NONE, M_GET);
				paths["/static/DataTables/media/js/jquery.dataTables.js"]
				    = ai(R_NONE, M_GET);

				// static - jquery
				paths["/static/jquery-dist/dist/jquery.min.js"]
				    = ai(R_NONE, M_GET);

				// static - js
				paths["/static/js/default/event-list.js"] = ai(R_USER, M_GET);
				paths["/static/js/default/index.js"] = ai(R_NONE, M_GET);
				paths["/static/js/default/settings.js"] = ai(R_USER, M_GET);
				paths["/static/js/event/add.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/event/details.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/event/list.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/event/update.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/group/add.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/group/details.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/group/list.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/group/update.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/user/add.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/user/list.js"] = ai(R_ADMIN, M_GET);
				paths["/static/js/user/login.js"] = ai(R_NONE, M_GET);
				paths["/static/js/user/update.js"] = ai(R_ADMIN, M_GET);

				// static - semantic
				paths["/static/semantic-ui-css/semantic.css"]
				    = ai(R_NONE, M_GET);
				paths["/static/semantic-ui-css/semantic.js"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "icons.ttf"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "icons.woff"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "icons.woff2"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "outline-icons.ttf"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "outline-icons.woff"]
				    = ai(R_NONE, M_GET);
				paths
				    ["/static/semantic-ui-css/themes/default/assets/fonts/"
				     "outline-icons.woff2"]
				    = ai(R_NONE, M_GET);

				// user
				paths["/user/add"] = ai(R_ADMIN, M_BOTH);
				paths["/user/details"] = ai(R_ADMIN, M_GET);
				paths["/user/list"] = ai(R_ADMIN, M_GET);
				paths["/user/list-data"] = ai(R_ADMIN, M_GET);
				paths["/user/login"] = ai(R_NONE, M_BOTH);
				paths["/user/logout"] = ai(R_USER, M_POST);
				paths["/user/remove"] = ai(R_ADMIN, M_POST);
				paths["/user/update"] = ai(R_ADMIN, M_GET);
				paths["/user/update-general"] = ai(R_ADMIN, M_POST);
				paths["/user/update-password"] = ai(R_ADMIN, M_POST);
			}

			// redirect if path is in redirs
			if(redirs.find(request.getPathInfo()) != redirs.end())
			{
				return controllers::send_status(reply,
				                                HTTP_TEMPORARY_REDIRECT,
				                                redirs[request.getPathInfo()]);
			}

			// redirect unknown paths to /default/not-found
			if(paths.find(request.getPathInfo()) == paths.end())
			{
				log_debug(std::string("Path not found: ")
				          + request.getPathInfo());
				return controllers::send_status(reply, HTTP_NOT_FOUND);
			}

			ai i = paths[request.getPathInfo()];

			// refuse all requests with wrong methods
			if(i.type == M_GET && !request.isMethodGET())
			{
				return controllers::send_status(
				    reply, HTTP_METHOD_NOT_ALLOWED, "GET");
			}
			else if(i.type == M_POST && !request.isMethodPOST())
			{
				return controllers::send_status(
				    reply, HTTP_METHOD_NOT_ALLOWED, "POST");
			}
			else if(!request.isMethodGET() && !request.isMethodPOST())
			{
				return controllers::send_status(
				    reply, HTTP_METHOD_NOT_ALLOWED, "GET, POST");
			}

			// set request var with rights
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());
			TNT_REQUEST_SHARED_VAR(int, user_rights, (0));
			if(session_user.get_is_admin())
				user_rights = 2;
			else if(session_user.is_logged_in())
				user_rights = 1;
			else
				user_rights = 0;

			// filter unauthorized requests
			if(i.rights <= user_rights)
				return DECLINED;
			else
			{
				return controllers::send_status(reply, HTTP_FORBIDDEN);
			}
		}
	} // namespace helpers
} // namespace tea
