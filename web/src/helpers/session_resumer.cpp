#include "session_resumer.hpp"

#include <tntdb/connection.h>

#include "../models/dal/session_resume_dal.hpp"
#include "../models/dal/user_dal.hpp"
#include "../models/user.hpp"

extern tntdb::Connection dbCon;

namespace tea
{
	namespace helpers
	{
		unsigned session_resumer::operator()(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			// get user from session
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			//if session was already not found
			if(session_user.get_id() == -2)
				return DECLINED;

			// if user is logged in
			if(session_user.get_id() >= 0)
				return DECLINED;

			// if user isn't logged in
			if(!request.hasCookie("lecasession"))
				return DECLINED;

			models::dal::session_resume_dal::remove_all_expired();
			// get session key
			std::string session_id = request.getCookie("lecasession");

			// try to get session user id from db
			int user_id;
			try
			{
				user_id = models::dal::session_resume_dal::load_by_identifier(
				    session_id);
			}
			catch(const std::exception& e)
			{
				session_user = models::user(-2, "not@fou.nd", "not found", "not found", false, false);
				return DECLINED;
			}

			// try to load user
			try
			{
				models::user us = models::dal::user_dal::load_by_id(user_id);
				if(!us.get_is_admin())
					session_user = us;
			}
			catch(const std::exception& e)
			{
				return DECLINED;
			}

			return DECLINED;
		}
	} // namespace helpers
} // namespace tea
