#pragma once

#include <map>

#include "../convenience/controllers.hpp"
namespace tea
{
	namespace helpers
	{
		class access_controll : public tnt::Component
		{
			log_define("tea.helpers.access-controll");

		private:
			enum method_types
			{
				M_BOTH,
				M_GET,
				M_POST
			};
			enum required_rights
			{
				R_NONE = 0,
				R_USER = 1,
				R_ADMIN = 2
			};
			struct access_information
			{
				method_types type;
				required_rights rights;
				access_information(required_rights rights = R_ADMIN,
				                   method_types type = M_GET);
			};

			typedef access_information ai;
			typedef std::map<std::string, access_information> paths_information;
			typedef std::map<std::string, std::string> redirect_paths;

			paths_information paths;
			redirect_paths redirs;

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<access_controll>
		    factory("tea/helper/access_controll");
	} // namespace helpers
} // namespace tea
