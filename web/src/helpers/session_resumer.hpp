#pragma once

#include <map>

#include "../convenience/controllers.hpp"
namespace tea
{
	namespace helpers
	{
		class session_resumer : public tnt::Component
		{
			log_define("tea.helpers.session-resumer");

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<session_resumer>
		    factory("tea/helper/session_resumer");
	} // namespace helpers
} // namespace tea
