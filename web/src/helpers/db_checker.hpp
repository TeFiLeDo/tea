#pragma once

#include "../convenience/controllers.hpp"
namespace tea
{
	namespace helpers
	{
		class db_checker : public tnt::Component
		{
			log_define("tea.helpers.db-checker");

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<db_checker>
		    factory("tea/helper/db_checker");
	} // namespace helpers
} // namespace tea
