#include "event.hpp"

namespace tea
{
	namespace models
	{
		event::event()
		    : event(-1,
		            "error",
		            "error",
		            tntdb::Datetime::localtime(),
		            "error",
		            false,
		            false)
		{}

		event::event(int id,
		             std::string title,
		             std::string description,
		             tntdb::Datetime time,
		             std::string location,
		             bool is_public,
		             bool is_for_all)
		{
			id_ = id;
			this->set_title(title);
			this->set_description(description);
			this->set_time(time);
			this->set_location(location);
			this->set_is_public(is_public);
			this->set_is_for_all(is_for_all);
		}

		int event::get_id()
		{
			return id_;
		}

		std::string event::get_title()
		{
			return title_;
		}

		std::string event::get_description()
		{
			return description_;
		}

		tntdb::Datetime event::get_time()
		{
			return time_;
		}

		std::string event::get_location()
		{
			return location_;
		}

		bool event::get_is_public()
		{
			return is_public_;
		}

		bool event::get_is_for_all()
		{
			return is_for_all_;
		}

		void event::set_title(std::string value)
		{
			if(value.length() > 0 && value.length() <= 30)
				title_ = value;
			else
				throw std::invalid_argument(
				    "Der Titel muss mindestens 1 Zeichen und maximal 30 "
				    "Zeichen lang sein.");
		}

		void event::set_description(std::string value)
		{
			description_ = value;
		}

		void event::set_time(tntdb::Datetime value)
		{
			time_ = value;
		}

		void event::set_location(std::string value)
		{
			location_ = value;
		}

		void event::set_is_public(bool value)
		{
			is_public_ = value;
		}

		void event::set_is_for_all(bool value)
		{
			is_for_all_ = value;
		}

		void event::serialize(cxxtools::SerializationInfo& si) const
		{
			si.addMember("id") <<= this->id_;
			si.addMember("title") <<= this->title_;
			si.addMember("time") <<= this->time_.getIso();
		}
	} // namespace models
} // namespace tea
