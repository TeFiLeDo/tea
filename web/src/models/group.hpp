#pragma once

#include <string>

#include <cxxtools/log.h>
#include <cxxtools/serializationinfo.h>

namespace tea
{
	namespace models
	{
		class group
		{
			log_define("tea.model.group");

		private:
			// fields
			int id_;
			std::string name_;

		public:
			// ctor
			group();
			group(int id, std::string name);

			// props - get
			int get_id();
			std::string get_name();

			// props - set
			void set_name(std::string value);

			// methods
			void serialize(cxxtools::SerializationInfo& si) const;
		};

		inline void operator<<=(cxxtools::SerializationInfo& si,
		                        const tea::models::group& gp)
		{
			gp.serialize(si);
		};
	} // namespace models
} // namespace tea
