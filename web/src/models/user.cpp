#include "user.hpp"

#include <regex>

namespace tea
{
	namespace models
	{
		user::user()
		    : user(-1, "error@errormail.error", "error", "error", false, false)
		{}

		user::user(int id,
		           std::string email,
		           std::string first_name,
		           std::string last_name,
		           bool is_admin,
		           bool is_locked_out)
		{
			id_ = id;
			this->set_email(email);
			this->set_first_name(first_name);
			this->set_last_name(last_name);
			this->set_is_admin(is_admin);
			this->set_is_locked_out(is_locked_out);
		}

		int user::get_id()
		{
			return id_;
		}

		std::string user::get_email()
		{
			return email_;
		}

		std::string user::get_first_name()
		{
			return first_name_;
		}

		std::string user::get_last_name()
		{
			return last_name_;
		}

		bool user::get_is_admin()
		{
			return is_admin_;
		}

		bool user::get_is_locked_out()
		{
			return is_locked_out_;
		}

		void user::set_email(std::string value)
		{
			static const std::regex email_reg(
			    "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/"
			    "=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-"
			    "\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-"
			    "\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*["
			    "a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]"
			    "|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|["
			    "01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-"
			    "\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-"
			    "\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

			if(std::regex_match(value, email_reg))
				email_ = value;
			else
				throw std::invalid_argument(
				    "Die angegebene Email-Adresse ist nicht gültig.");
		}

		void user::set_first_name(std::string value)
		{
			first_name_ = value;
		}

		void user::set_last_name(std::string value)
		{
			last_name_ = value;
		}

		void user::set_is_admin(bool value)
		{
			is_admin_ = value;
		}

		void user::set_is_locked_out(bool value)
		{
			is_locked_out_ = value;
		}

		bool user::is_logged_in()
		{
			return id_ > 0;
		}

		std::string user::printable_name()
		{
			if(!first_name_.length() && !last_name_.length())
				return email_;
			else if(!first_name_.length())
				return last_name_;
			else if(!last_name_.length())
				return first_name_;
			else
				return first_name_ + " " + last_name_;
		}

		void user::serialize(cxxtools::SerializationInfo& si) const
		{
			si.addMember("id") <<= this->id_;
			si.addMember("email") <<= this->email_;
			si.addMember("firstName") <<= this->first_name_;
			si.addMember("lastName") <<= this->last_name_;
		}
	} // namespace models
} // namespace tea
