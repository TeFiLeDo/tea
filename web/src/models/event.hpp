#pragma once

#include <string>

#include <cxxtools/log.h>
#include <cxxtools/serializationinfo.h>

#include <tntdb/datetime.h>

namespace tea
{
	namespace models
	{
		class event
		{
			log_define("tea.model.event");

		private:
			int id_;
			std::string title_;
			std::string description_;
			tntdb::Datetime time_;
			std::string location_;
			bool is_public_;
			bool is_for_all_;

		public:
			// ctors
			event();
			event(int id,
			      std::string title,
			      std::string description,
			      tntdb::Datetime time,
			      std::string location,
			      bool is_public,
			      bool is_for_all);

			// props - get
			int get_id();
			std::string get_title();
			std::string get_description();
			tntdb::Datetime get_time();
			std::string get_location();
			bool get_is_public();
			bool get_is_for_all();

			// props - set
			void set_description(std::string value);
			void set_time(tntdb::Datetime value);
			void set_title(std::string value);
			void set_location(std::string value);
			void set_is_public(bool value);
			void set_is_for_all(bool value);

			// methods
			void serialize(cxxtools::SerializationInfo& si) const;
		};

		inline void operator<<=(cxxtools::SerializationInfo& si,
		                        const tea::models::event& ev)
		{
			ev.serialize(si);
		}
	} // namespace models
} // namespace tea
