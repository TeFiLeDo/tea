#pragma once

#include <list>

#include <tntdb/row.h>

#include "../event.hpp"

namespace tea
{
	namespace models
	{
		namespace dal
		{
			class event_dal
			{
				log_define("tea.models.dal.event");

			private:
				static models::event to_event(tntdb::Row row);

			public:
				// crud - c
				static models::event add(models::event data,
				                         std::list<int> user_ids,
				                         std::list<int> group_ids);
				static void add_group(int event_id, int group_id);
				static void add_user(int event_id, int user_id);
				// crud - r
				static std::list<models::event> load_all();
				static std::list<models::event> load_all_of_actual_month();
				static models::event load_by_id(int event_id);
				static std::list<models::event>
				load_by_ids(std::list<int> event_ids);
				static std::list<int> load_groups_anti_by_id(int event_id);
				static std::list<int> load_groups_by_id(int event_id);
				static std::list<int> load_users_anti_by_id(int event_id);
				static std::list<int> load_users_by_id(int event_id);
				// crud - u
				static void update(models::event data);
				// crud - d
				static void remove(int id);
				static void remove_group(int event_id, int group_id);
				static void remove_user(int event_id, int user_id);
			};
		} // namespace dal
	}     // namespace models
} // namespace tea
