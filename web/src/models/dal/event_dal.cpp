#include "event_dal.hpp"

#include <tntdb/connection.h>
#include <tntdb/datetime.h>
#include <tntdb/error.h>
#include <tntdb/result.h>
#include <tntdb/statement.h>

#include "../../convenience/error.hpp"

extern tntdb::Connection dbCon;

namespace tea
{
	namespace models
	{
		namespace dal
		{
			models::event event_dal::to_event(tntdb::Row row)
			{
				try
				{
					return models::event(
					    row.getInt("id"),
					    row.getString("title"),
					    row.getString("description"),
					    row.getDatetime("time"),
					    row.getString("location"),
					    row.getInt(
					        "isPublic") /*tried to use tntdb::Row::getBool(),
					                       but it simply doesn't work*/
					    ,
					    row.getInt("isForAll"));
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to convert row to event. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Das Event konnte nicht geladen werden.");
				}
			}

			models::event event_dal::add(models::event data,
			                             std::list<int> user_ids,
			                             std::list<int> group_ids)
			{
				// insert new row
				static tntdb::Statement stm_insert = dbCon.prepare(
				    "INSERT INTO events (title, description, time, location, "
				    "isPublic, isForAll) VALUES (:title, :description, :time, "
				    ":location, :is_public, :is_for_all);");
				try
				{
					stm_insert.setString("title", data.get_title())
					    .setString("description", data.get_description())
					    .setDatetime("time", data.get_time())
					    .setString("location", data.get_location())
					    .setBool("is_public", data.get_is_public())
					    .setBool("is_for_all", data.get_is_for_all())
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to add new event to DB. Message: ")
					    + e.what());
					throw error(409 /*conflict*/,
					            "Das Event konnte nicht erstellt werden.");
				}

				// get inserted id
				static tntdb::Statement stm_get_last = dbCon.prepare(
				    "SELECT id FROM events WHERE title = :title AND time = "
				    ":time;");
				int id = 0;
				try
				{
					id = stm_get_last.setString("title", data.get_title())
					         .setDatetime("time", data.get_time())
					         .selectValue()
					         .getInt();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to get id of newest event in "
					                      "DB. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Nach der Erstellung des Events ist ein Fehler "
					            "aufgetreten. Die ausgewählten Benutzer und "
					            "Gruppen wurden nicht eingeladen.");
				}

				// insert users
				for(auto i = user_ids.begin(); i != user_ids.end(); i++)
				{
					try
					{
						add_user(id, *i);
					}
					catch(...)
					{
						log_warn(
						    "Failed to add an user to an event, continueing "
						    "with next user.");
					}
				}

				// insert groups
				for(auto i = group_ids.begin(); i != group_ids.end(); i++)
				{
					try
					{
						add_group(id, *i);
					}
					catch(...)
					{
						log_warn(
						    "Failed to add a group to an event, continueing "
						    "with next group.");
					}
				}

				return load_by_id(id);
			}

			void event_dal::add_group(int event_id, int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "INSERT INTO groupevent VALUES (:group_id, :event_id);");

				try
				{
					stm.setInt("group_id", group_id)
					    .setInt("event_id", event_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to add group to event "
					                      "invitation list. Message: ")
					          + e.what());
					throw error(
					    500 /*internal server error*/,
					    "Die Gruppe konnte nicht zum Event eingeladen werden.");
				}
			}

			void event_dal::add_user(int event_id, int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "INSERT INTO userevent VALUES (:user_id, :event_id);");

				try
				{
					stm.setInt("user_id", user_id)
					    .setInt("event_id", event_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to add user to event "
					                      "invitation list. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Der Benutzer konnte nicht zum event "
					            "eingeladen werden.");
				}
			}

			std::list<models::event> event_dal::load_all()
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM events;");

				tntdb::Result res;
				try
				{
					res = stm.select();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to load all events. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden aller Events ist fehlgeschlagen.");
				}

				std::list<models::event> evs;
				for(auto i = res.begin(); i != res.end(); i++)
					evs.push_back(to_event(*i));

				return evs;
			}

			std::list<models::event> event_dal::load_all_of_actual_month()
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT * FROM events WHERE YEAR(time) = "
				    "YEAR(NOW()) AND MONTH(time) = MONTH(NOW()) AND isPublic = "
				    "true;");

				tntdb::Result res;
				try
				{
					res = stm.select();
				}
				catch(const std::exception& e)
				{
					log_error(std::string("Failed to load all events of "
					                      "current month. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden aller Events des aktuellen Monats "
					            "ist fehlgeschlagen.");
				}

				std::list<models::event> ret;
				for(auto i = res.begin(); i != res.end(); i++)
					ret.push_back(to_event(*i));

				return ret;
			}

			models::event event_dal::load_by_id(int event_id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM events WHERE id = :id;");

				try
				{
					tntdb::Row res = stm.setInt("id", event_id).selectRow();
					return to_event(res);
				}
				catch(tntdb::NotFound& e)
				{
					log_error("Failed to load event, id not found.");
					throw error(404 /*not found*/,
					            "Das Event konnte nicht gefunden werden.");
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden des Events ist fehlgeschlagen.");
				}
			}

			std::list<models::event>
			event_dal::load_by_ids(std::list<int> event_ids)
			{
				std::list<models::event> ret;

				for(auto i = event_ids.begin(); i != event_ids.end(); i++)
				{
					try
					{
						ret.push_back(load_by_id(*i));
					}
					catch(tea::error& e)
					{
						if(e.get_http_code() == 404)
							log_warn(
							    "Failed to load one event of a list. "
							    "Continueing with rest of list.");
						else
							throw error(500 /*internal server error*/,
							            "Das Laden der Events ist aus "
							            "unbekannten Gründen fehlgeschlagen.");
					}
				}

				return ret;
			}

			std::list<int> event_dal::load_groups_anti_by_id(int event_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM groups WHERE id NOT IN (SELECT groupId "
				    "FROM groupevent WHERE eventId = :event_id);");

				try
				{
					tntdb::Result res
					    = stm.setInt("event_id", event_id).select();
					std::list<int> ids;
					for(auto i = res.begin(); i != res.end(); i++)
						ids.push_back(i->getInt("id"));
					return ids;
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of groups not "
					                      "invited to event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Gruppen, die nicht zum Event eingeladen "
					            "sind, konnten nicht geladen werden.");
				}
			}

			std::list<int> event_dal::load_groups_by_id(int event_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT groupId FROM groupevent WHERE eventId = :id;");

				try
				{
					tntdb::Result res = stm.setInt("id", event_id).select();
					std::list<int> ids;
					for(auto i = res.begin(); i != res.end(); i++)
						ids.push_back(i->getInt("groupId"));
					return ids;
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of groups "
					                      "invited to event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Gruppen, die zum Event eingeladen sind, "
					            "konnten nicht geladen werden.");
				}
			}

			std::list<int> event_dal::load_users_anti_by_id(int event_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM users WHERE id NOT IN (SELECT userId FROM "
				    "userevent WHERE eventId = :event_id);");

				try
				{
					tntdb::Result res
					    = stm.setInt("event_id", event_id).select();
					std::list<int> ids;
					for(auto i = res.begin(); i != res.end(); i++)
						ids.push_back(i->getInt("id"));
					return ids;
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of users not "
					                      "invited to event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Benutzer, die nicht zum Event eingeladen "
					            "sind, konnten nicht geladen werden.");
				}
			}

			std::list<int> event_dal::load_users_by_id(int event_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT userId FROM userevent WHERE eventId = :id;");

				try
				{
					tntdb::Result res = stm.setInt("id", event_id).select();
					std::list<int> ids;
					for(auto i = res.begin(); i != res.end(); i++)
						ids.push_back(i->getInt("userId"));
					return ids;
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of users invited "
					                      "to event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Benutzer, die zum Event eingeladen sind, "
					            "konnten nicht geladen werden.");
				}
			}

			void event_dal::update(models::event data)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "UPDATE events SET title = :title, description = "
				    ":description, time = :time, location = :location, "
				    "isPublic = :is_public, isForAll = :is_for_all WHERE id = "
				    ":id;");

				try
				{
					stm.setString("title", data.get_title())
					    .setString("description", data.get_description())
					    .setDatetime("time", data.get_time())
					    .setString("location", data.get_location())
					    .setBool("is_public", data.get_is_public())
					    .setBool("is_for_all", data.get_is_for_all())
					    .setInt("id", data.get_id())
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to update event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Event konnte nicht geändert werden.");
				}
			}

			void event_dal::remove(int id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("DELETE FROM events WHERE id = :id;");

				try
				{
					stm.setInt("id", id).execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to remove event. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Event konnte nicht gelöscht werden.");
				}
			}

			void event_dal::remove_group(int event_id, int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM groupevent WHERE groupId = :group_id AND "
				    "eventId = :event_id;");

				try
				{
					stm.setInt("group_id", group_id)
					    .setInt("event_id", event_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to remove group from event "
					                      "invitation list. Message: ")
					          + e.what());
					throw error(
					    500 /*internal server error*/,
					    "Die Gruppe konnte nicht vom Event ausgeladen werden.");
				}
			}

			void event_dal::remove_user(int event_id, int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM userevent WHERE userId = :user_id AND eventId "
				    "= :event_id;");

				try
				{
					stm.setInt("user_id", user_id)
					    .setInt("event_id", event_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to remove user from event "
					                      "invitation list. Message: ")
					          + e.what());
				}
			}
		} // namespace dal
	}     // namespace models
} // namespace tea
