#pragma once

#include <list>

#include <tntdb/row.h>

#include "../user.hpp"

namespace tea
{
	namespace models
	{
		namespace dal
		{
			class user_dal
			{
				log_define("tea.models.dal.user");

			private:
				static models::user to_user(tntdb::Row row);
				static std::list<models::user> to_users(tntdb::Result res);

			public:
				// crud - c
				static models::user add(models::user data);
				// crud - r
				static std::list<models::user> load_all();
				static models::user
				load_by_email_if_authenticated(std::string email,
				                               std::string password);
				static models::user load_by_id(int user_id);
				static std::list<models::user>
				load_by_ids(std::list<int> user_ids);
				static std::list<int> load_events_anti_by_id(int user_id);
				static std::list<int> load_events_by_id(int user_id);
				static std::list<int> load_groups_anti_by_id(int user_id);
				static std::list<int> load_groups_by_id(int user_id);
				// crud - u
				static void update(models::user data);
				static void update_password(int user_id, std::string password);
				// crud - d
				static void remove(int user_id);
			};
		} // namespace dal
	}     // namespace models
} // namespace tea
