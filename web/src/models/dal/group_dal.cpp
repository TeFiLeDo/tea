#include "group_dal.hpp"

#include <tntdb/connection.h>
#include <tntdb/error.h>
#include <tntdb/result.h>
#include <tntdb/statement.h>

#include "../../convenience/error.hpp"

extern tntdb::Connection dbCon;

namespace tea
{
	namespace models
	{
		namespace dal
		{
			models::group group_dal::to_group(tntdb::Row row)
			{
				try
				{
					return models::group(row.getInt("id"),
					                     row.getString("name"));
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to convert row to group. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Die Gruppe konnte nicht geladen werden.");
				}
			}

			models::group group_dal::add(models::group gr, std::list<int> users)
			{
				static tntdb::Statement stm_insert
				    = dbCon.prepare("INSERT INTO groups VALUES (null, :name);");

				try
				{
					stm_insert.setString("name", gr.get_name()).execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to add new group. Message: ")
					          + e.what());
					throw error(409 /*conflict*/,
					            "Der eingegebene Name wird bereits von einer "
					            "anderen Gruppe verwendet.");
				}

				static tntdb::Statement stm_get_last = dbCon.prepare(
				    "SELECT id FROM groups WHERE name = :name;");
				int id;

				try
				{
					id = stm_get_last.setString("name", gr.get_name())
					         .selectValue()
					         .getInt();
				}
				catch(std::exception& e)
				{
					log_error(std::string(
					              "Failed to get id of newest group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Nach dem Erstellen der Gruppe ist ein "
					            "unerwarteter Fehler aufgetreten.");
				}

				for(auto i = users.begin(); i != users.end(); i++)
					add_user(id, *i);

				return load_by_id(id);
			}

			void group_dal::add_user(int group_id, int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "INSERT INTO usergroup VALUES (:user, :group);");

				try
				{
					stm.setInt("user", user_id)
					    .setInt("group", group_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to add user to group. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Der Benutzer konnte nicht zur Gruppe "
					            "hinzugefügt werden.");
				}
			}

			std::list<models::group> group_dal::load_all()
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM groups;");

				tntdb::Result res;

				try
				{
					res = stm.select();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to load all groups. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden aller Gruppen ist fehlgeschlagen.");
				}

				std::list<models::group> groups;
				for(auto i = res.begin(); i != res.end(); i++)
					groups.push_back(to_group(*i));

				return groups;
			}

			models::group group_dal::load_by_id(int group_id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM groups WHERE id = :id;");

				tntdb::Row res;

				try
				{
					res = stm.setInt("id", group_id).selectRow();
				}
				catch(tntdb::NotFound& e)
				{
					log_error("Failed to load group, id not found.");
					throw error(404 /*not found*/,
					            "Es konnte keine Gruppe zur angegebenen ID "
					            "gefunden werden.");
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load group. Message: ")
					          + e.what());
					throw error(
					    500 /*internal server error*/,
					    "Beim Laden der Gruppe ist ein Fehler aufgetreten.");
				}

				return to_group(res);
			}

			std::list<models::group>
			group_dal::load_by_ids(std::list<int> group_ids)
			{
				std::list<models::group> groups;
				for(auto i = group_ids.begin(); i != group_ids.end(); i++)
					groups.push_back(load_by_id(*i));
				return groups;
			}

			std::list<int> group_dal::load_events_anti_by_id(int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM events WHERE id NOT IN (SELECT eventId "
				    "FROM groupevent WHERE groupId = :group_id);");

				tntdb::Result res;

				try
				{
					res = stm.setInt("group_id", group_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load events that group "
					                      "is not invited to "
					                      ". Messge: ")
					          + e.what());
					return std::list<int>();
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("id"));
				return ids;
			}

			std::list<int> group_dal::load_events_by_id(int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT eventId FROM groupevent WHERE groupId = "
				    ":group_id;");

				tntdb::Result res;

				try
				{
					res = stm.setInt("group_id", group_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load events that group is "
					                      "invited to. Message: ")
					          + e.what());
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("eventId"));

				return ids;
			}

			std::list<int> group_dal::load_users_anti_by_id(int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM users WHERE id NOT IN (SELECT userId "
				    "FROM usergroup WHERE groupId = :group_id);");

				tntdb::Result res;

				try
				{
					res = stm.setInt("group_id", group_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of users that "
					                      "are not part of group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden der Benutzer-IDs, die nicht zur "
					            "Gruppe gehören, ist fehlgeschlagen.");
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("id"));

				return ids;
			}

			std::list<int> group_dal::load_users_by_id(int group_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT userId FROM usergroup WHERE groupId = :group_id;");

				tntdb::Result res;

				try
				{
					res = stm.setInt("group_id", group_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load ids of users that "
					                      "are part of group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden der Benutzer, die zur Gruppe "
					            "gehören, ist fehlgeschlagen.");
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("userId"));

				return ids;
			}

			void group_dal::update(models::group data)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "UPDATE groups SET name = :name WHERE id = :group_id;");

				try
				{
					stm.setString("name", data.get_name())
					    .setInt("group_id", data.get_id())
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to update group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Gruppe konnte nicht aktualisiert werden.");
				}
			}

			void group_dal::remove(int id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("DELETE FROM groups WHERE id = :group_id;");

				try
				{
					stm.setInt("group_id", id).execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to remove group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Gruppe konnte nicht gelöscht werden.");
				}
			}

			void group_dal::remove_user(int group_id, int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM usergroup WHERE userId = :user_id AND groupId "
				    "= :group_id;");

				try
				{
					stm.setInt("user_id", user_id)
					    .setInt("group_id", group_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string(
					              "Failed to remove user from group. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Der Benutzer konnte nicht aus der Gruppe "
					            "entfernt werden.");
				}
			}
		} // namespace dal
	}     // namespace models
} // namespace tea
