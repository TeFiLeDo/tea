#pragma once

#include <list>

#include <tntdb/row.h>

#include "../group.hpp"

namespace tea
{
	namespace models
	{
		namespace dal
		{
			class group_dal
			{
				log_define("tea.model.dal.group");

			private:
				static models::group to_group(tntdb::Row row);

			public:
				// crud - c
				static models::group add(models::group gr_data,
				                         std::list<int> users
				                         = std::list<int>());
				static void add_user(int group_id, int user_id);
				// crud - r
				static std::list<models::group> load_all();
				static models::group load_by_id(int group_id);
				static std::list<models::group>
				load_by_ids(std::list<int> group_ids);
				static std::list<int> load_events_anti_by_id(int group_id);
				static std::list<int> load_events_by_id(int group_id);
				static std::list<int> load_users_anti_by_id(int group_id);
				static std::list<int> load_users_by_id(int group_id);
				// crud - u
				static void update(models::group data);
				// crud - d
				static void remove(int id);
				static void remove_user(int group_id, int user_id);
			};
		} // namespace dal
	}     // namespace models
} // namespace tea
