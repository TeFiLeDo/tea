#include "user_dal.hpp"

#include <tntdb/connection.h>
#include <tntdb/datetime.h>
#include <tntdb/error.h>
#include <tntdb/result.h>
#include <tntdb/statement.h>

#include "../../convenience/error.hpp"

extern tntdb::Connection dbCon;

namespace tea
{
	namespace models
	{
		namespace dal
		{
			models::user user_dal::to_user(tntdb::Row row)
			{
				try
				{
					return models::user(
					    row.getInt("id"),
					    row.getString("email"),
					    row.getString("firstName"),
					    row.getString("lastName"),
					    row.getInt(
					        "isAdmin"), // tried to use tntdb::Row::getBoll()
					                    // but it simply doesn't work.
					    row.getInt("isLockedOut"));
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to convert row to user. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden eines Benutzers ist aufgrund eines "
					            "internen Fehlers fehlgeschlagen.");
				}
			}

			std::list<models::user> user_dal::to_users(tntdb::Result res)
			{
				std::list<models::user> ret;
				for(auto i = res.begin(); i != res.end(); i++)
					ret.push_back(to_user(*i));
				return ret;
			}

			models::user user_dal::add(models::user data)
			{
				// insert new user into db
				static tntdb::Statement stm_insert = dbCon.prepare(
				    "INSERT INTO users (email, firstName, lastName, password, "
				    "passwordSalt, isAdmin, isLockedOut) VALUES (:email, "
				    ":first_name, :last_name, SHA2(CONCAT(\"asdf\", "
				    "\"abcdefg\"), 256), \"abcdefg\", :is_admin, "
				    ":is_locked_out);");
				try
				{
					stm_insert.setString("email", data.get_email())
					    .setString("first_name", data.get_first_name())
					    .setString("last_name", data.get_last_name())
					    .setBool("is_admin", data.get_is_admin())
					    .setBool("is_locked_out", data.get_is_locked_out())
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to add new user. Message: ")
					          + e.what());
					throw error(409 /*conflict*/,
					            "Die eingegebene Email-Adresse wird bereits "
					            "von einem anderen Benutzer verwendet.");
				}

				// return new user
				try
				{
					return load_by_email_if_authenticated(data.get_email(),
					                                      "asdf");
				}
				catch(tea::error& e)
				{
					throw error(500 /*internal server error*/,
					            "Nach der Erstellung des neuen Benutzers trat "
					            "ein unerwarteter Fehler auf.");
				}
			}

			std::list<models::user> user_dal::load_all()
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM users;");

				tntdb::Result res;
				try
				{
					res = stm.select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load all users. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Ein unerwarteter Fehler ist beim Laden aller "
					            "Benutzer aufgetreten.");
				}

				return to_users(res);
			}

			models::user
			user_dal::load_by_email_if_authenticated(std::string email,
			                                         std::string password)
			{
				tntdb::Statement stm_get = dbCon.prepare(
				    "SELECT * FROM users WHERE email = :email AND password = "
				    "SHA2(CONCAT(:pw, passwordSalt), 256) AND isLockedOut = "
				    "false;");

				tntdb::Result res;
				try
				{
					res = stm_get.setString("email", email)
					          .setString("pw", password)
					          .select();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to authenticate user. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Die Anmeldung ist aus technischen Gründen "
					            "fehlgeschlagen.");
				}

				if(res.empty())
				{
					log_info("Invalid login detected.");
					throw error(
					    403 /*internal server error*/,
					    "Die eingegebenen Daten konnten nicht bestätigt "
					    "werden. Wenn sie ihr Passwort vergessen haben oder "
					    "trotz korrekter Anmelde-Daten diesen Fehler bekommen, "
					    "dann setzten sie sich bitte mit ihrem Administrator "
					    "in Verbindung.");
				}

				return to_user(*res.begin());
			}

			models::user user_dal::load_by_id(int user_id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("SELECT * FROM users WHERE id = :id;");

				tntdb::Row res;
				try
				{
					res = stm.setInt("id", user_id).selectRow();
				}
				catch(tntdb::NotFound& e)
				{
					log_error(std::string(
					    "Failed to load user by id because id was not found."));
					throw error(404 /*not found*/,
					            "Es konnte kein Benutzer zur angegebenen ID "
					            "gefunden werden.");
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to load user by id. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Das Laden des Benutzers ist aus unbekannten "
					            "Gründen fehlgeschlagen.");
				}

				return to_user(res);
			}

			std::list<models::user>
			user_dal::load_by_ids(std::list<int> user_ids)
			{
				std::list<models::user> ret;

				for(auto i = user_ids.begin(); i != user_ids.end(); i++)
				{
					try
					{
						ret.push_back(load_by_id(*i));
					}
					catch(tea::error& e)
					{
						if(e.get_http_code() == 404)
							log_warn(
							    "Failed to load one user of a list. "
							    "Continueing with the rest of the list.");
						else
							throw error(500 /*internal server error*/,
							            "Das Laden der Benutzer ist aus "
							            "unbekannten Gründen fehlgeschlagen.");
					}
				}

				return ret;
			}

			std::list<int> user_dal::load_events_anti_by_id(int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM events WHERE id NOT IN (SELECT eventId "
				    "FROM userevent WHERE userId = :user_id);");

				tntdb::Result res;

				try
				{
					res = stm.setInt("user_id", user_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load events that user "
					                      "is not invited to "
					                      ". Messge: ")
					          + e.what());
					return std::list<int>();
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("id"));
				return ids;
			}

			std::list<int> user_dal::load_events_by_id(int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT eventId FROM userevent WHERE userId = "
				    ":user_id;");

				tntdb::Result res;

				try
				{
					res = stm.setInt("user_id", user_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load events that user is "
					                      "invited to. Message: ")
					          + e.what());
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("eventId"));

				return ids;
			}

			std::list<int> user_dal::load_groups_anti_by_id(int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT id FROM groups WHERE id NOT IN (SELECT groupId "
				    "FROM usergroup WHERE userId = :user_id);");

				tntdb::Result res;

				try
				{
					res = stm.setInt("user_id", user_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load groups that user is "
					                      "not a member of. Message: ")
					          + e.what());
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("id"));

				return ids;
			}

			std::list<int> user_dal::load_groups_by_id(int user_id)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "SELECT groupId FROM usergroup WHERE userId = "
				    ":user_id;");

				tntdb::Result res;

				try
				{
					res = stm.setInt("user_id", user_id).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load groups that user is "
					                      "part of. Message: ")
					          + e.what());
				}

				std::list<int> ids;
				for(auto i = res.begin(); i != res.end(); i++)
					ids.push_back(i->getInt("groupId"));

				return ids;
			}

			void user_dal::update(models::user data)
			{
				static tntdb::Statement stm = dbCon.prepare(
				    "UPDATE users SET firstName = :first_name, lastName = "
				    ":last_name, isAdmin = :is_admin, isLockedOut = "
				    ":is_locked_out WHERE id = :id;");

				try
				{
					stm.setString("first_name", data.get_first_name())
					    .setString("last_name", data.get_last_name())
					    .setBool("is_admin", data.get_is_admin())
					    .setBool("is_locked_out", data.get_is_locked_out())
					    .setInt("id", data.get_id())
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Failed to update user object. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Die Daten des Benutzers konnten nicht "
					            "bearbeitet werden.");
				}
			}

			void user_dal::update_password(int user_id, std::string password)
			{
				// check if password is long enough
				if(password.length() < 8)
					throw error(400,
					            "Ein gültiges Passwort muss mindestens 8 "
					            "Zeichen lang sein.");

				// define some char categorys that must be represented in a
				// valid password
				const std::string capitalts = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				const std::string letters = "abcdefghijklmnopqrstuvwxyz";
				const std::string numbers = "0123456789";
				const std::string syms
				    = "^´+#<,.-°!\"§$%&/()=?`*';:_>{[]}\\@€~|";

				// check if password contains all categorys
				bool has_capitals = false;
				bool has_letters = false;
				bool has_numbers = false;
				bool has_syms = false;
				for(auto i = password.begin(); i != password.end(); i++)
				{
					if(!has_capitals)
						if(capitalts.find(*i) != std::string::npos)
						{
							has_capitals = true;
							continue;
						}
					if(!has_letters)
						if(letters.find(*i) != std::string::npos)
						{
							has_letters = true;
							continue;
						}
					if(!has_numbers)
						if(numbers.find(*i) != std::string::npos)
						{
							has_numbers = true;
							continue;
						}
					if(!has_syms)
						if(syms.find(*i) != std::string::npos)
						{
							has_syms = true;
							continue;
						}
				}
				if(!has_capitals || !has_letters || !has_numbers || !has_syms)
					throw error(400,
					            "Ein gültiges Passwort muss Groß- und "
					            "Kleinbuchstaben, Nummern und Sonderzeichen "
					            "(^´+#<,.-°!\\\"§$%&/()=?`*';:_>{[]}\\\\@€~|) "
					            "bestehen.");

				// generate salt
				std::srand(time(0));
				std::rand();
				std::rand();
				std::rand();
				const std::string salt_syms = capitalts + letters + numbers;
				std::string salt;
				for(int i = 0; i < 256; i++)
					salt += salt_syms[std::rand() % 62];

				// update password in DB
				static tntdb::Statement stm = dbCon.prepare(
				    "UPDATE users SET passwordSalt = :salt, password = "
				    "SHA2(CONCAT(:pw, :salt), 256) WHERE id = :id;");

				try
				{
					stm.setString("salt", salt)
					    .setString("pw", password)
					    .setInt("id", user_id)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string(
					        "Failed to update password for user. Message: ")
					    + e.what());
					throw error(
					    500 /*internal server error*/,
					    "Das Passwort konnte nicht aktualisiert werden.");
				}
			}

			void user_dal::remove(int user_id)
			{
				static tntdb::Statement stm
				    = dbCon.prepare("DELETE FROM users WHERE id = :id;");

				try
				{
					stm.setInt("id", user_id).execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to remove user. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Der Benutzer konnte nicht gelöscht werden.");
				}
			}
		} // namespace dal
	}     // namespace models
} // namespace tea
