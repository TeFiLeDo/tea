#include "session_resume_dal.hpp"

#include <cxxtools/serializationinfo.h>

#include <tntdb/connection.h>
#include <tntdb/error.h>
#include <tntdb/result.h>
#include <tntdb/statement.h>

#include "../../convenience/error.hpp"
#include "user_dal.hpp"

extern tntdb::Connection dbCon;

namespace tea
{
	namespace models
	{
		namespace dal
		{
			std::string session_resume_dal::add(int user_id)
			{
				// generate random identifier
				std::srand(time(0));
				std::rand();
				std::rand();
				std::rand();
				const std::string syms
				    = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				      "abcdefghijklmnopqrstuvwxyz"
				      "0123456789";
				std::string identifier;
				for(int i = 0; i < 256; i++)
					identifier += syms[std::rand() % 62];

				// insert new row
				static tntdb::Statement stm = dbCon.prepare(
				    "INSERT INTO sessionResume (userId, identifier, "
				    "passwordHash, creation) VALUES (:user_id, :identifier, "
				    "(SELECT password FROM users WHERE id = :user_id), "
				    "NOW());");

				try
				{
					stm.setInt("user_id", user_id)
					    .setString("identifier", identifier)
					    .execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string(
					              "Failed to add new session to DB. Message: ")
					          + e.what());
					throw error(409 /*conflict*/,
					            "Die Session konnte nicht gespeichert werden.");
				}

				return identifier;
			}

			int session_resume_dal::load_by_identifier(std::string identifier)
			{
				tntdb::Statement stm = dbCon.prepare(
				    "SELECT * FROM sessionResume WHERE identifier = "
				    ":identifier;");

				tntdb::Result res;
				try
				{
					res = stm.setString("identifier", identifier).select();
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to load session. Message:")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Die Session konnte nicht geladen werden.");
				}

				if(res.empty())
					throw error(404 /*not found*/,
					            "Die Session konnte nicht gefunden werden.");

				// check if password has been changed
				try
				{
					tntdb::Statement stm_check = dbCon.prepare(
					    "SELECT * FROM users WHERE id = :id "
					    "AND password = :password;");

					tntdb::Result res2
					    = stm_check.setInt("id", res.begin()->getInt("userId"))
					          .setString("password",
					                     res.begin()->getString("passwordHash"))
					          .select();

					if(res2.empty())
						throw error(403 /*forbidden*/,
						            "Das Passwort wurde zwischenzeitlich "
						            "geändert.");
				}
				catch(std::exception& e)
				{
					log_error(std::string("Failed to validate session user "
					                      "password. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Das Passwort konnte nicht bestätigt werden.");
				}

				try
				{
					return res.begin()->getInt("userId");
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string("Loaded invalid session row. Message: ")
					    + e.what());

					throw error(500 /*internal server error*/,
					            "Die Session konnte nicht geladen werden.");
				}
			}

			void session_resume_dal::remove_all_expired()
			{
				tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM sessionResume WHERE YEAR(creation) != "
				    "YEAR(NOW()) AND MONTH(creation) != MONTH(NOW());");

				try
				{
					stm.execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string(
					        "Failed to remove expired sessions. Message: ")
					    + e.what());
					throw error(500 /*internal server error*/,
					            "Ausgelaufene Sessions konnten nicht "
					            "gelöscht werden.");
				}
			}

			void session_resume_dal::remove_all_for_user(int user_id)
			{
				tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM sessionResume WHERE userId = :user_id;");

				try
				{
					stm.setInt("user_id", user_id).execute();
				}
				catch(std::exception& e)
				{
					log_error(
					    std::string(
					        "Failed to remove sessions for user. Message: ")
					    + e.what());

					throw error(500 /*internal server error*/,
					            "Sessions für Benutzer konnten nicht "
					            "gelöscht werden.");
				}
			}

			void session_resume_dal::remove_by_id(std::string identifier)
			{
				tntdb::Statement stm = dbCon.prepare(
				    "DELETE FROM sessionResume WHERE identifier = :id;");

				try
				{
					stm.setString("id", identifier).execute();
				}
				catch(std::exception& e)
				{
					log_error(std::string(
					              "Failed to remove session with id. Message: ")
					          + e.what());
					throw error(500 /*internal server error*/,
					            "Session konnte nicht gelöscht werden.");
				}
			}
		} // namespace dal
	}     // namespace models
} // namespace tea
