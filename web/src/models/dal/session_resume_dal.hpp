#pragma once

#include <string>

#include <cxxtools/log.h>

namespace tea
{
	namespace models
	{
		namespace dal
		{
			class session_resume_dal
			{
				log_define("tea.models.dal.session_resume");

			public:
				// crud - c
				static std::string add(int user_id);

				// crud - r
				static int load_by_identifier(std::string session_identifier);

				// crud - d
				static void remove_all_expired();
				static void remove_all_for_user(int user_id);
				static void remove_by_id(std::string identifier);
			};
		} // namespace dal
	}     // namespace models
} // namespace tea
