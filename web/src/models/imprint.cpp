#include "imprint.hpp"

namespace tea
{
	namespace models
	{
		imprint::imprint()
		    : imprint("",
		              std::list<std::string>(),
		              std::list<std::string>(),
		              std::list<std::string>(),
		              "",
		              std::list<std::string>())
		{}

		imprint::imprint(std::string name,
		                 std::list<std::string> address,
		                 std::list<std::string> emails,
		                 std::list<std::string> phones,
		                 std::string zvr,
		                 std::list<std::string> opening)
		    : name_(name),
		      address_(address),
		      emails_(emails),
		      phones_(phones),
		      zvr_(zvr),
		      opening_(opening)
		{}

		std::string imprint::get_name()
		{
			return name_;
		}

		std::list<std::string> imprint::get_address()
		{
			return address_;
		}

		std::list<std::string> imprint::get_emails()
		{
			return emails_;
		}

		std::list<std::string> imprint::get_phones()
		{
			return phones_;
		}

		std::string imprint::get_zvr()
		{
			return zvr_;
		}

		std::list<std::string> imprint::get_opening()
		{
			return opening_;
		}

		void imprint::deserialize(const cxxtools::SerializationInfo& si)
		{
			si.getMember("name") >>= name_;
			si.getMember("address") >>= address_;
			si.getMember("emails") >>= emails_;
			if(si.findMember("phones"))
				si.getMember("phones") >>= phones_;
			si.getMember("zvr") >>= zvr_;
			if(si.findMember("opening"))
				si.getMember("opening") >>= opening_;
		}
	} // namespace models
} // namespace tea
