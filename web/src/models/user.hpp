#pragma once

#include <string>

#include <cxxtools/log.h>
#include <cxxtools/serializationinfo.h>

namespace tea
{
	namespace models
	{
		class user
		{
			log_define("tea.model.user");

		private:
			int id_;
			std::string email_;
			std::string first_name_;
			std::string last_name_;
			bool is_admin_;
			bool is_locked_out_;

		public:
			// ctor
			user();
			user(int id,
			     std::string email,
			     std::string first_name,
			     std::string last_name,
			     bool is_admin,
			     bool is_locked_out);

			// props - get
			int get_id();
			std::string get_email();
			std::string get_first_name();
			std::string get_last_name();
			bool get_is_admin();
			bool get_is_locked_out();

			// props - set
			void set_email(std::string value);
			void set_first_name(std::string value);
			void set_last_name(std::string value);
			void set_is_admin(bool value);
			void set_is_locked_out(bool value);

			// methods
			bool is_logged_in();
			std::string printable_name();

			void serialize(cxxtools::SerializationInfo& si) const;
		};

		inline void operator<<=(cxxtools::SerializationInfo& si,
		                        const tea::models::user& us)
		{
			us.serialize(si);
		}

	} // namespace models
} // namespace tea
