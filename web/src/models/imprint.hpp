#pragma once

#include <list>
#include <string>

#include <cxxtools/log.h>
#include <cxxtools/serializationinfo.h>

namespace tea
{
	namespace models
	{
		class imprint
		{
			log_define("tea.models.imprint");

		private:
			// vars
			std::string name_;
			std::list<std::string> address_;
			std::list<std::string> emails_;
			std::list<std::string> phones_;
			std::string zvr_;
			std::list<std::string> opening_;

		public:
			// ctors
			imprint();
			imprint(std::string name,
			        std::list<std::string> address,
			        std::list<std::string> emails,
			        std::list<std::string> phones,
			        std::string zvr,
			        std::list<std::string> opening);

			// props - get
			std::string get_name();
			std::list<std::string> get_address();
			std::list<std::string> get_emails();
			std::list<std::string> get_phones();
			std::string get_zvr();
			std::list<std::string> get_opening();

			// methods
			void deserialize(const cxxtools::SerializationInfo& si);
		};

		inline void operator>>=(const cxxtools::SerializationInfo& si,
		                        imprint& i)
		{
			i.deserialize(si);
		}
	} // namespace models
} // namespace tea
