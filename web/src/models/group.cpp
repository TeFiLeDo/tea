#include "group.hpp"

namespace tea
{
	namespace models
	{
		group::group() : group(-1, "error") {}

		group::group(int id, std::string name) : id_(id)
		{
			set_name(name);
		}

		int group::get_id()
		{
			return id_;
		}

		std::string group::get_name()
		{
			return name_;
		}

		void group::set_name(std::string value)
		{
			if(value.length() < 1 || value.length() > 30)
				throw std::invalid_argument(
				    "Der Name einer Gruppe muss zwichen 1 und 30 Zeichen "
				    "haben.");
			name_ = value;
		}

		void group::serialize(cxxtools::SerializationInfo& si) const
		{
			si.addMember("id") <<= id_;
			si.addMember("name") <<= name_;
		}
	} // namespace models
} // namespace tea
