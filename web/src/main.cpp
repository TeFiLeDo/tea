#include <fstream>
#include <iostream>

#include <cxxtools/json.h>
#include <cxxtools/log.h>

#include <tnt/tntconfig.h>
#include <tnt/tntnet.h>

#include <tntdb/connect.h>
#include <tntdb/connection.h>

log_define("tea");

tnt::Tntnet app;
tntdb::Connection dbCon;

int main(int argc, char* argv[])
{
	// initialize logging
	try
	{
		log_init();
		log_info("APPLICATION START");
	}
	catch(std::exception& e)
	{
		std::cout << "ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	tnt::TntConfig& conf = tnt::TntConfig::it();

	// load config
	try
	{
		log_debug("Load config.");
		std::ifstream i("tea.json");
		i >> cxxtools::Json(conf);
		app.init(conf);
		log_info("Loaded configuration");
	}
	catch(std::exception& e)
	{
		log_fatal(std::string("Error when loading config: ") + e.what());
		log_info("APPLICATION STOP");
		std::cout << "CONFIGURATION ERRRO: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	// connect to db
	try
	{
		log_debug("Connection to db.");
		dbCon = tntdb::connect(conf.environment.at("dbString"));
		log_info("Connected to db.");
	}
	catch(std::exception& e)
	{
		log_fatal(std::string("Error while connecting to db: ") + e.what());
		log_info("APPLICATION STOP");
		std::cout << "DATABASE ERROR: " << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	// map urls
	app.mapUrl("", "tea/helper/db_checker"); // check if database is available.
	if(conf.environment.find("useUnsafeSessionResumer")
	   != conf.environment.end())
		if(conf.environment.at("useUnsafeSessionResumer")
		   == "yesUseUnsafeSessionResumer")
			app.mapUrl(
			    "", "tea/helper/session_resumer"); // resumes expired sessions
	app.mapUrl("",
	           "tea/helper/access_controll"); // check existance for all paths.
	app.mapUrl("^/static/(.*)$",
	           "tea/static/$1"); // calls a static component if a static file is
	                             // requested.
	app.mapUrl("^/default/(.*)$", "tea/controller/default")
	    .setPathInfo("$1"); // calls the default controller if a page starting
	                        // with /default/ is requested.
	app.mapUrl("^/event/(.*)$", "tea/controller/event")
	    .setPathInfo("$1"); // calls the user controller if a page starting with
	                        // /event/ is requested.
	app.mapUrl("^/group/(.*)$", "tea/controller/group")
	    .setPathInfo("$1"); // calls the group controller if a page starting
	                        // with /group/ is requested.
	app.mapUrl("^/user/(.*)$", "tea/controller/user")
	    .setPathInfo("$1"); // calls the user controller if a page starting with
	                        // /user/ is requested.
	app.mapUrl("^/(.*)$", "tea/view/shared/layout")
	    .setPathInfo("$1"); // calls the default layout.
	app.mapUrl("^/(.*)$", "tea/view/shared/centerer")
	    .setPathInfo("$1"); // calls the centerer layout.
	app.mapUrl("^/(.*)$",
	           "tea/view/$1"); // if no layout matches a views requirements, the
	                           // view itself is called.

	log_info("Launching server.");
	app.run();
	log_info("Server finished.");

	log_info("APPLICATION STOP");
	return EXIT_SUCCESS;
}
