function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

//general settings
var us_gen_email = $('#us-gen-email');
var us_gen_name_first = $('#us-gen-name-first');
var us_gen_name_last = $('#us-gen-name-last');
var us_gen_locked = $('#us-gen-locked').dropdown();
var us_gen_rank = $('#us-gen-rank').dropdown();

us_gen_email.val(us_gen_email.attr("placeholder"));

var us_gen = $('#us-gen').form({
	"inline": true,
	"on": "change",
	"fields": {
		"us-gen-name-first": {
			"rules": [
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		},
		"us-gen-name-first": {
			"rules": [
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		}
	}
});

function updateGeneral() {
	$.ajax({
		"data": {
			"firstname": us_gen_name_first.val(),
			"id": findGetParameter("id"),
			"lastname": us_gen_name_last.val(),
			"locked": us_gen_locked.dropdown('get value'),
			"rank": us_gen_rank.dropdown('get value')
		},
		"dataType": "json",
		"success": function() {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Der Benutzer wurde erfolgreich geändert.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/user/update-general"
	});
}
//!general settings

//password change
var us_pass_pw = $('#us-pass-pw');
var us_pass_repeat = $('#us-pass-repeat');

var us_pass = $('#us-pass').form({
	"inline": true,
	"on": "change",
	"fields": {
		"us-pass-pw": {
			"rules": [
				{
					"type": "minLength[8]",
					"prompt": "Ein Passwort muss mindestens 8 Zeichen lang sein."
				},
				{
					"type": "regExp[/^.*[a-z].*$/]",
					"prompt": "Ein Passwort muss mindestens einen Kleinbuchstaben beinhalten."
				},
				{
					"type": "regExp[/^.*[A-Z].*$/]",
					"prompt": "Ein Passwort muss mindestens einen Großbuchstaben beinhalten."
				},
				{
					"type": "regExp[/^.*[0-9].*$/]",
					"prompt": "Ein Passwort muss mindestens eine Zahl beinhalten."
				}
			]
		},
		"us-pass-repeat": {
			"rules": [
				{
					"type": "match[us-pass-pw]",
					"prompt": "Passwörter müssen übereinstimmen."
				}
			]
		}
	}
});

function updatePassword() {
	$.ajax({
		"complete": function() {
			us_pass.form('clear');
		},
		"data": {
			"id": findGetParameter("id"),
			"password": us_pass_pw.val()
		},
		"dataType": "json",
		"success": function() {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Das Passwort wurde erfolgreich geändert.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/user/update-password"
	});
}
//!password change
