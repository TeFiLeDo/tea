var us_table = $('#us-table').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "email",
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a href="mailto:' + data + '">' + data + '</a>';
			}
		},
		{
			"data": "firstName"
		},
		{
			"data": "lastName"
		},
		{
			"data": null,
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/user/details?id=' + data.id + '"><i class="info icon"></i></a>' +
					'<a class="ui icon button" href="mailto:' + data.email + '"><i class="envelope icon"></i></a>' +
					'<a class="ui icon button" href="/user/update?id=' + data.id + '"><i class="pencil icon"></i></a>' + 
					'<button class="ui icon button" onclick="remove_approval(' + data.id + ')"><i class="alternate trash icon"></i></button>'
					;
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/user/list-data",
		"dataSrc": ""
	}
});

function remove(id) {
	$.ajax({
		"data": {
			"id": id
		},
		"dataType": "text",
		"success": function() {
			us_table.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/user/remove"
	});
}

function remove_approval(id) {
	$('#delete-modal').modal({
		"closable": false,
		"onApprove": function() {
			remove(id);
		}
	}).modal('show');
}
