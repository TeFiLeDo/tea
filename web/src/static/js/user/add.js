var us_locked = $('#us-locked').dropdown();
var us_rank = $('#us-rank').dropdown();

var us_form = $('#us-form').form({
	"inline": true,
	"on": "change",
	"fields": {
		"us-email": {
			"rules": [
				{
					"type": "email",
					"prompt": "Muss eine gültige Email-Adresse sein."
				}
			]
		}
	}
});

function add() {
	$.ajax({
		"data": {
			"email": $('#us-email').val(),
			"firstname": $('#us-name-first').val(),
			"lastname": $('#us-name-last').val(),
			"admin": us_rank.dropdown('get value'),
			"locked": us_locked.dropdown('get value')
		},
		"dataType": "json",
		"success": function() {
			us_form.form('clear');
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Der Benutzer wurde erfolgreich erstellt.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/user/add"
	});
}
