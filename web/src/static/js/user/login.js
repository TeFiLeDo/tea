function handle_submit() {
	$.ajax({
		"url": '/user/login',
		"data": {
			"email": $('#login-email').val(),
			"password": $('#login-password').val()
		},
		"dataType": 'text',
		"type": 'POST',
		"timeout": 5000,
		"success": function(res) {
			document.location.href = '/';
		}
	});
}

$('#login').form({
	"inline": true,
	"on": 'change',
	"fields": {
		"login-email": {
			"rules": [
				{
					"type": 'email',
					"prompt": "Muss eine gültige Email-Adresse sein."
				}
			]
		},
		"login-password": {
			"rules": [
				{
					"type": 'empty',
					"prompt": "Darf nicht leer sein."
				}
			]
		}
	}
});
