$('#gr-users').dropdown();

$('#gr-form').form({
	"inline": true,
	"on": "change",
	"fields": {
		"gr-name": {
			"rules": [
				{
					"type": "empty",
					"prompt": "Darf nicht leer sein."
				},
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		}
	}
});

function add() {
	$.ajax({
		"data": {
			"name": $('#gr-name').val(),
			"users": $('#gr-users').dropdown('get value')
		},
		"dataType": "text",
		"success": function() {
			$('#gr-form').form('clear');
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Die Gruppe wurde erfolgreich erstellt.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/group/add"
	});
};
