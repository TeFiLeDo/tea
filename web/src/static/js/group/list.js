var gr_table;

function remove(id){
	$.ajax({
		"data":	{
			"id": id
		},
		"dataType": "text",
		"success": function() {
			gr_table.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/group/remove"
	});
}

function remove_approval(id)
{
	$('#delete-modal').modal({
		"closable": false,
		"onApprove": function() {
			remove(id);
		}
	}).modal('show');
}

gr_table = $('#gr-table').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": null,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data.name;
				return '<a href="/group/details?id=' + data.id + '">' + data.name + '</a>';
			}
		},
		{
			"data": "id",
			"orderable": false,
			"render": function(data, type, row, meta)  {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/group/details?id=' + data + '"><i class="info icon"></i></a>' +
					'<a class="ui icon button" href="/group/update?id=' + data + '"><i class="pencil icon"></i></a>' +
					'<button class="ui icon button" onclick="remove_approval(' + data + ')"><i class="alternate trash icon"></i></button>'
				;
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/group/list-data",
		"dataSrc": ""
	}
});
