function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

//mail button
var mail_button = $('<a class="link item" href="mailto:"></a>');
mail_button.html('<i class="envelope icon"></i>Email an Mitglieder');
$('#corner-menu').prepend(mail_button);
var mail_list = {};

function update_mail_button_href() {
	var href = "mailto:";

	for(var key in mail_list) {
		if(!mail_list.hasOwnProperty(key))
			continue;
		if(href.length > 7)
			href += ",";
		href += key;
	}

	mail_button.attr("href", href);
}
//!mail button

//user table
var gr_us_table = $('#gr-us-table').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "email",
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a href="mailto:' + data + '">' + data + '</a>';
			}
		},
		{
			"data": "firstName"
		},
		{
			"data": "lastName"
		},
		{
			"data": null,
			"orderable": false,
			"render": function(data, type, row, meta) {
				var l = Object.keys(mail_list).length;
				if(!(data.mail in mail_list))
					mail_list[data.email] = 0;
				if(l != Object.keys(mail_list).length)
					update_mail_button_href();
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/user/details?id=' + data.id + '"><i class="info icon"></i></a><a class="ui icon button" href="mailto:' + data.email + '"><i class="envelope icon"></i></a>';
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/group/details-user-list?id=" + findGetParameter("id"),
		"dataSrc": ""
	}
});
//!user table

