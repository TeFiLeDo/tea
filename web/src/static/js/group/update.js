function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

//general settings
function updateGeneral()
{
	$.ajax({
		"data": {
			"id": findGetParameter("id"),
			"name": $('#gr-gen-name').val()
		},
		"dataType": "json", "success": function() {  
			var grGenName = $('#gr-gen-name');
			if(grGenName.val().length > 0) {
				grGenName.attr("placeholder", grGenName.val());
			}
			$('#gr-gen').form('clear');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/group/update-general"
	});
}

$('#gr-gen').form({
	"inline": true,
	"on": "change",
	"fields": {
		"gr-gen-name": {
			"rules": [
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		}
	}
});
//!general settings

//user settings
//user table
var gr_us_table = $('#gr-us-table').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "email"
		},
		{
			"data": "firstName"
		},
		{
			"data": "lastName"
		},
		{
			"data": "id",
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<button class="ui icon button" type="button" onclick="updateUserRemove(' + data + ')"><i class="trash alternate icon"></i></button>';
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/group/details-user-list?id=" + findGetParameter("id"),
		"dataSrc": ""
	}
});
//!user table
//add user
var gr_us_add_input = $('#gr-us-add-input').dropdown({
	"apiSettings": {
		onResponse: function (resp) {
			var data = resp.data;
			var res = {};
			res.success = true;
			res.results = [];
			for(var i = 0; i < data.length; i++)
			{
				var line = {};
				line.value = data[i].id;
				if(data[i].firstName.length == 0 && data[i].lastName.lenth == 0)
					line.name = data[i].email;
				else if(data[i].firstName.length == 0)
					line.name = data[i].lastName;
				else if(data[i].lastName.length == 0)
					line.name = data[i].firstName;
				else
					line.name = data[i].firstName + " " + data[i].lastName;
				
				res.results.push(line);
			}
			return res;
		},
		cache: false,
		url: "/group/update-user-add?id=" + findGetParameter("id")
	},
	filterRemoteData: true
});

function updateUserAdd() {
	$.ajax({
		"data": {
			"id": findGetParameter("id"),
			"user": gr_us_add_input.dropdown('get value')
		},
		"dataType": "json",
		"success": function()
		{
			gr_us_add_input.dropdown('refresh');
			gr_us_add_input.dropdown('restore defaults');
			gr_us_table.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/group/update-user-add"
	});
}
//!add user
//remove user
function updateUserRemove(userId) {
	$.ajax({
		"data": {
			"id": findGetParameter("id"),
			"user": userId
		},
		"dataType": "json",
		"success": function()
		{
			gr_us_table.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/group/update-user-remove"
	});
}
//!remove user
//!user settings
