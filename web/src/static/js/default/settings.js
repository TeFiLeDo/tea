//name
var us_name_first = $('#us-name-first');
var us_name_last = $('#us-name-last');

var us_name = $('#us-name').form();

function updateNames() {
	$.ajax({
		data: {
			firstname: us_name_first.val(),
			lastname: us_name_last.val()
		},
		dataType: "json",
		success: function () {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Dein Name wurde erfolgreich geändert!");
			$('#error-modal').modal('show');
		},
		timeout: 5000,
		type: "POST",
		url: "/default/settings-username"
	});
}
//!name

//password
var us_pw_new = $('#us-pw-n');
var us_pw_old = $('#us-pw-o');
var us_pw_repeat = $('us-pw-r');

var us_pw = $('#us-pw').form({
	inline: true,
	on: "change",
	fields: {
		"us-pw-o": {
			rules: [
				{
					type: "empty",
					promt: "Dieses Feld ist erforderlich."
				}
			]
		},
		"us-pw-n": {
			rules: [
				{
					type: "minLength[8]",
					prompt: "Ein Passwort muss mindestens 8 Zeichen lang sein."
				},
				{
					type: "regExp[/^.*[a-z].*$/]",
					prompt: "Ein Passwort muss mindestens einen Kleinbuchstaben beinhalten."
				},
				{
					type: "regExp[/^.*[A-Z].*$/]",
					prompt: "Ein Passwort muss mindestens einen Großbuchstaben beinhalten."
				},
				{
					type: "regExp[/^.*[0-9].*$/]",
					prompt: "Ein Passwort muss mindestens eine Zahl beinhalten."
				}
			]
		},
		"us-pw-r": {
			rules: [
				{
					type: "match[us-pw-n]",
					promt: "Die neuen Passwörter stimmen nicht überein."
				}
			]
		}
	}
});

function updatePw() {
	$.ajax({
		data: {
			pwn: us_pw_new.val(),
			pwo: us_pw_old.val()
		},
		dataType: "json",
		success: function () {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Dein Passwort wurde erfolgreich geändert!");
			$('#error-modal').modal('show');
		},
		timeout: 5000,
		type: "POST",
		url: "/default/settings-password"
	});
}
//!password

function storeSession() {
	$.ajax({
		success: function () {
			$('#error-header').text("Erfolg!");
			$('#error-message').html("<p>Deine akutelle Session wurde erfolgreich gespeichert.</p><p>Bitte beachte, dass dieses Feature von deiner Organisation ausgeschaltet werden kann.</p><p>Beachte bitte auch, dass dieses Feature die Sicherheit deiner Daten einschränkt. (Deshalb ist es für Administratoren auch nicht verfügbar.)</p><p>Um es zu deaktivieren, ändere bitte dein Passwort.</p>");
			$('#error-modal').modal('show');
		},
		timeout: 5000,
		type: "POST",
		url: "/default/settings-store-session"
	});
}

var store_button = $('<a class="link item" onclick="storeSession()"></a>');
var store_icon = $('<i class="save icon"></i>');
store_button.append(store_icon);
store_button.append("Session speichern");
$('#corner-menu').prepend(store_button);
