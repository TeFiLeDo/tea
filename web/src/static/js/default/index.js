var ev_table = $('#ev-table').DataTable({
	ajax: {
		url: "/default/index-list-data",
		dataSrc: ""
	},
	columns: [
		{
			data: null,
			render: function (data, type, row, meta) {
				if (type != "display")
					return data.title;
				return `<a href="/default/event-details?id=${data.id}">${data.title}</a>`;
			}
		},
		{
			data: "time"
		}
	]
});
