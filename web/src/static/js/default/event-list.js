var ev_table = $('#ev-table').DataTable({
	ajax: {
		url: "/default/event-list-data",
		dataSrc: ""
	},
	columns: [
		{
			data: "id"
		},
		{
			data: "title"
		},
		{
			data: "time"
		},
		{
			data: "id",
			orderable: false,
			render: function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/default/event-details?id=' + data + '"><i class="info icon"></i></a>';
			},
			searchable: false
		}
	],
	order: [
		[2, "asc"]
	]
});

function change_list() {
	if(ev_table.ajax.url() == "/default/event-list-data") {
		ev_table.ajax.url("/default/event-list-data?passed=true");
		change_icon.removeClass("slash");
	}
	else {
		ev_table.ajax.url("/default/event-list-data");
		change_icon.addClass("slash");
	}

	ev_table.ajax.reload(null, true);
}

var change_button = $('<a class="link item" onclick="change_list()"></a>');
var change_icon = $('<i class="eye slash icon"></i>');
change_button.append(change_icon);
change_button.append("Vergangene Events");
$('#corner-menu').prepend(change_button);
