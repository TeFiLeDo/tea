//get all fields
var ev_all = $('#ev-all').dropdown();
var ev_date_day = $('#ev-date-day').dropdown({
	"onChange": function() {
		checkDate();
	}
});
var ev_date_month = $('#ev-date-month').dropdown({
	"onChange": function() {
		checkDate();
	}
});
var ev_date_year = $('#ev-date-year').dropdown({
	"onChange": function() {
		checkDate();
	}
});
var ev_description = $('#ev-description');
var ev_destination = $('#ev-destination');
var ev_groups = $('#ev-groups').dropdown();
var ev_public = $('#ev-public').dropdown();
var ev_time_hour = $('#ev-time-hour').dropdown();
var ev_time_min = $('#ev-time-min').dropdown();
var ev_title = $('#ev-title');
var ev_users = $('#ev-users').dropdown();

var ev = $('#ev').form({
	"inline": true,
	"on": "change",
	"fields": {
		"ev-title": {
			"rules": [
				{
					"type": "empty",
					"prompt": "Darf nicht leer sein."
				},
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		}
	}
});

//check if selected day exists
function checkDate() {
	var day = ev_date_day.dropdown('get value');
	var month = ev_date_month.dropdown('get value');
	var year = ev_date_year.dropdown('get value');

	//let all months with 31 days pass
	if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
		return;

	//check months with 30 days
	if(month == 4 || month == 6 || month == 9 || month == 11)
		if(day <= 30)
			return;

	//check february
	if(month == 2) {
		if(year % 4 == 0) {
			if(day <= 29)
				return;
		}
		else {
			if(day <= 28)
				return;
		}
	}

	//if day doesn't exist
	$('#error-header').text("Fehlerhafte Eingabe!");
	$('#error-message').text("Der eingestellte Tag existiert nicht.");
	$('#error-modal').modal('show');
}

function add() {
	$.ajax({
		"data": {
			"all": ev_all.dropdown('get value'),
			"date-day": ev_date_day.dropdown('get value'),
			"date-month": ev_date_month.dropdown('get value'),
			"date-year": ev_date_year.dropdown('get value'),
			"description": ev_description.val(),
			"destination": ev_destination.val(),
			"groups": ev_groups.dropdown('get value'),
			"public": ev_public.dropdown('get value'),
			"time-hour": ev_time_hour.dropdown('get value'),
			"time-min": ev_time_min.dropdown('get value'),
			"title": ev_title.val(),
			"users": ev_users.dropdown('get value')
		},
		"dataType": "text",
		"success": function() {
			ev.form('reset');
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Das Event wurde erfolgreich erstellt.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/event/add"
	});
}
