function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
} 
//general settings
var ev_gen_all = $('#ev-gen-all').dropdown();
var ev_gen_title = $('#ev-gen-title');
var ev_gen_date_day = $('#ev-gen-date-day').dropdown();
var ev_gen_date_month = $('#ev-gen-date-month').dropdown();
var ev_gen_date_year = $('#ev-gen-date-year').dropdown();
var ev_gen_public = $('#ev-gen-public').dropdown();
var ev_gen_time_hour = $('#ev-gen-time-hour').dropdown();
var ev_gen_time_min = $('#ev-gen-time-min').dropdown();

var ev_gen = $('#ev-gen').form({
	"inline": true,
	"on": "change",
	"fields": {
		"ev-gen-title": {
			"rules": [
				{
					"type": "empty",
					"prompt": "Darf nicht leer sein."
				},
				{
					"type": "maxLength[30]",
					"prompt": "Darf nicht länger als 30 Zeichen sein."
				}
			]
		}
	}
});

function updateGen() {
	$.ajax({
		"data": {
			"all": ev_gen_all.dropdown('get value'),
			"date-day": ev_gen_date_day.dropdown('get value'),
			"date-month": ev_gen_date_month.dropdown('get value'),
			"date-year": ev_gen_date_year.dropdown('get value'),
			"id": findGetParameter("id"),
			"public": ev_gen_public.dropdown('get value'),
			"time-hour": ev_gen_time_hour.dropdown('get value'),
			"time-min": ev_gen_time_min.dropdown('get value'),
			"title": ev_gen_title.val()
		},
		"dataType": "json",
		"success": function() {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Das Event wurde erfolgreich geändert.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/event/update-general"
	});
}
//!general settings

//detail settings
var ev_det_desc = $('#ev-det-desc');
var ev_det_loc = $('#ev-det-loc');

var ev_det = $('#ev-det').form();

function updateDetails() {
	$.ajax({
		"data": {
			"description": ev_det_desc.val(),
			"id": findGetParameter("id"),
			"location": ev_det_loc.val()
		},
		"success": function() {
			$('#error-header').text("Erfolg!");
			$('#error-message').text("Das Event wurde erfolgreich geändert.");
			$('#error-modal').modal('show');
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/event/update-detail-info"
	});
}
//!detail settings

//user settings
var ev_us_add = $('#ev-us-add').dropdown({
	apiSettings: {
		onResponse: function (resp) {
			var data = resp.data;
			var res = {};
			res.success = true;
			res.results = [];
			for(var i = 0; i < data.length; i++)
			{
				var line = {};
				line.value = data[i].id;
				if(data[i].firstName.length == 0 && data[i].lastName.lenth == 0)
					line.name = data[i].email;
				else if(data[i].firstName.length == 0)
					line.name = data[i].lastName;
				else if(data[i].lastName.length == 0)
					line.name = data[i].firstName;
				else
					line.name = data[i].firstName + " " + data[i].lastName;
				
				res.results.push(line);
			}
			return res;
		},
		cache: false,
		url: "/event/update-user-add?id=" + findGetParameter("id"),
	},
	filterRemoteData: true
});

var ev_us_tab = $('#ev-us-tab').DataTable({
	"ajax": {
		"url": "/event/details-users?id=" + findGetParameter("id"),
		"dataSrc": ""
	},
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "email"
		},
		{
			"data": "firstName"
		},
		{
			"data": "lastName"
		},
		{
			"data": "id",
			"orderable": false,
			"render": function (data, type, row, meta) {
				if(type != "display")
					return data;
				return '<button class="ui icon button" type="button" onclick="updateUserRemove(' + data + ')"><i class="trash alternate icon"></i></button>';
			},
			"searchable": false
		}
	],
	"order": [
		[1, "asc"]
	]
});

function updateUserAdd() {
	$.ajax({
		"data": {
			"id": findGetParameter("id"),
			"user": ev_us_add.dropdown('get value')
		},
		"dataType": "json",
		"success": function() {
			ev_us_add.dropdown('restore defaults');
			ev_us_tab.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/event/update-user-add"
	});
}

function updateUserRemove(userId) {
	$.ajax({
		"data": {
			"id": findGetParameter("id"),
			"user": userId
		},
		"dataType": "json",
		"success": function() {
			ev_us_tab.ajax.reload(null, false);
		},
		"timeout": 5000,
		"type": "POST",
		"url": "/event/update-user-remove"
	});
}
//!user settings

//group settings
var ev_gr_add = $('#ev-gr-add').dropdown({
	apiSettings: {
		onResponse: function(resp) {
			var data = resp.data;
			var res = {};
			res.success = true;
			res.results = [];
			for(var i = 0; i < data.length; i++) {
				var line = {};
				line.value = data[i].id;
				line.name = data[i].name;
				res.results.push(line);
			}
			return res;
		},
		cache: false,
		url: "/event/update-group-add?id=" + findGetParameter("id")
	},
	filterRemoteData: true
});

var ev_gr_tab = $('#ev-gr-tab').DataTable({
	ajax: {
		url: "/event/details-groups?id=" + findGetParameter("id"),
		dataSrc: ""
	},
	columns: [
		{
			data: "id"
		},
		{
			data: "name"
		},
		{
			data: "id",
			orderable: false,
			render: function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<button class="ui icon button" type="button" onclick="updateGroupRemove(' + data + ')"><i class="trash alternate icon"></i></button>';
			},
			searchable: false
		}
	],
	order: [
		[1, "asc"]
	]
});

function updateGroupAdd() {
	$.ajax({
		data: {
			id: findGetParameter("id"),
			group: ev_gr_add.dropdown('get value')
		},
		dataType: "json",
		success: function() {
			ev_gr_add.dropdown('restore defaults');
			ev_gr_tab.ajax.reload(null, false);
		},
		timeout: 5000,
		type: "POST",
		url: "/event/update-group-add"
	});
}

function updateGroupRemove(groupId) {
	$.ajax({
		data: {
			id: findGetParameter("id"),
			group: groupId
		},
		dataType: "json",
		success: function() {
			ev_gr_tab.ajax.reload(null, false);
		},
		timeout: 5000,
		type: "POST",
		url: "/event/update-group-remove"
	});
}
//!group settings
