var ev_table = $('#ev-table').DataTable({
	"ajax": {
		"url": "/event/list-data",
		"dataSrc": ""
	},
	"columns": [
		{
			"data": "id"
		},
		{
			"data": null,
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data.title;
				return '<a href="/event/details?id=' + data.id + '">' + data.title + '</a>';
			},
			"searchable": false
		},
		{
			"data": "time"
		},
		{
			"data": "id",
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/event/details?id=' + data + '"><i class="info icon"></i></a>' +
					'<a class="ui icon button" href="/event/update?id=' + data + '"><i class="pencil icon"></i></a>' +
					'<button class="ui icon button" onclick="remove(' + data + ')"><i class="alternate trash icon"></i></button>'
				;
			},
			"searchable": false
		}
	],
	"order": [
		[2, "asc"]
	]
});

function remove(id) {
	$('#delete-modal').modal({
		closable: false,
		onApprove: function() {
			$.ajax({
				data: {
					id: id
				},
				dataaType: "text",
				success: function() {
					ev_table.ajax.reload(null, false);
				},
				timeout: 5000,
				type: "POST",
				url: "/event/remove"
			});
		}
	}).modal('show');
}
