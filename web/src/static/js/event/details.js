function findGetParameter(parameterName) {
	var result = null, tmp = [];
	location.search.substr(1).split("&").forEach(function (item) {
		tmp = item.split("=");
		if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
	});
	return result;
}

//user table
var ev_us = $('#ev-us').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "email",
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a href="mailto:' + data + '">' + data + '</a>';
			}
		},
		{
			"data": "firstName"
		},
		{
			"data": "lastName"
		},
		{
			"data": null,
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/user/details?id=' + data.id + '"><i class="info icon"></i></a>' +
					'<a class="ui icon button" href="mailto:' + data.email + '"><i class="envelope icon"></i></a>'
				;
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/event/details-users?id=" + findGetParameter("id"),
		"dataSrc": ""
	}
});


//group table
var ev_gr = $('#ev-gr').DataTable({
	"columns": [
		{
			"data": "id"
		},
		{
			"data": "name"
		},
		{
			"data": "id",
			"orderable": false,
			"render": function(data, type, row, meta) {
				if(type != "display")
					return data;
				return '<a class="ui icon button" href="/group/details?id=' + data + '"><i class="info icon"></i></a>'
				;
			},
			"searchable": false
		}
	],
	"ajax": {
		"url": "/event/details-groups?id=" + findGetParameter("id"),
		"dataSrc": ""
	}
});
