#include "event_controller.hpp"

#include "../convenience/error.hpp"
#include "../models/dal/event_dal.hpp"
#include "../models/dal/group_dal.hpp"
#include "../models/dal/user_dal.hpp"
#include "../models/event.hpp"
#include "../models/group.hpp"
#include "../models/user.hpp"

namespace tea
{
	namespace controllers
	{
		unsigned event_controller::add(tnt::HttpRequest& request,
		                               tnt::HttpReply& reply,
		                               tnt::QueryParams& qparam)
		{
			if(request.isMethodGET())
			{
				std::list<models::user> users;
				try
				{
					users = models::dal::user_dal::load_all();
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}

				std::list<models::group> groups;
				try
				{
					groups = models::dal::group_dal::load_all();
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}

				TNT_REQUEST_SHARED_VAR(std::list<tea::models::user>, us, ());
				us = users;
				TNT_REQUEST_SHARED_VAR(std::list<tea::models::group>, gr, ());
				gr = groups;

				return DECLINED;
			}
			else
			{
				// set return header
				reply.setHeader("Content-Type", "text/json; charset=UTF-8");

				// for error handling
				unsigned error;

				// get all values
				bool all = get_bool(reply, qparam, error, "all", true);
				if(error)
					return error;
				int date_day = get_int(reply, qparam, error, "date-day", true);
				if(error)
					return error;
				int date_month
				    = get_int(reply, qparam, error, "date-month", true);
				if(error)
					return error;
				int date_year
				    = get_int(reply, qparam, error, "date-year", true);
				if(error)
					return error;
				std::string description = get_string(
				    reply, qparam, error, "description", true, true);
				if(error)
					return error;
				std::string destination = get_string(
				    reply, qparam, error, "destination", true, true);
				if(error)
					return error;
				std::string groups_string
				    = get_string(reply, qparam, error, "groups", true, true);
				if(error)
					return error;
				bool publicity = get_bool(reply, qparam, error, "public", true);
				if(error)
					return error;
				int time_hour
				    = get_int(reply, qparam, error, "time-hour", true);
				if(error)
					return error;
				int time_min = get_int(reply, qparam, error, "time-min", true);
				if(error)
					return error;
				std::string title
				    = get_string(reply, qparam, error, "title", true);
				if(error)
					return error;
				std::string users_string
				    = get_string(reply, qparam, error, "users", true, true);
				if(error)
					return error;
				// got all values

				// check date
				try
				{
					check_date(date_year, date_month, date_day);
				}
				catch(std::exception& e)
				{
					return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
				}
				//! check date

				// check time
				try
				{
					check_time(time_hour, time_min);
				}
				catch(std::exception& e)
				{
					return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
				}
				//! check time

				// split lists
				std::list<int> groups_pre = split_int_list(groups_string);
				std::list<int> users_pre = split_int_list(users_string);

				// validate lists
				std::list<int> groups;
				for(auto i = groups_pre.begin(); i != groups_pre.end(); i++)
				{
					try
					{
						groups.push_back(
						    models::dal::group_dal::load_by_id(*i).get_id());
					}
					catch(tea::error& e)
					{
						if(e.get_http_code() == 404)
							continue;
						else
							return send_status(
							    reply, e.get_http_code(), e.what(), true);
					}
				}

				std::list<int> users;
				for(auto i = users_pre.begin(); i != users_pre.end(); i++)
				{
					try
					{
						users.push_back(
						    models::dal::user_dal::load_by_id(*i).get_id());
					}
					catch(tea::error& e)
					{
						if(e.get_http_code() == 404)
							continue;
						else
							return send_status(
							    reply, e.get_http_code(), e.what(), true);
					}
				}

				// add event
				models::event ev;
				try
				{
					ev = models::event(-1,
					                   title,
					                   description,
					                   tntdb::Datetime(date_year,
					                                   date_month,
					                                   date_day,
					                                   time_hour,
					                                   time_min,
					                                   0),
					                   destination,
					                   publicity,
					                   all);
				}
				catch(std::exception& e)
				{
					return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
				}

				try
				{
					models::dal::event_dal::add(ev, users, groups);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				return HTTP_NO_CONTENT;
			}
		}

		unsigned event_controller::details(tnt::HttpRequest& request,
		                                   tnt::HttpReply& reply,
		                                   tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			// load data
			models::event ev_data;
			try
			{
				ev_data = models::dal::event_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save data in request
			TNT_REQUEST_SHARED_VAR(tea::models::event, ev, ());
			ev = ev_data;

			// return to proceed
			return DECLINED;
		}

		unsigned event_controller::details_groups(tnt::HttpRequest& request,
		                                          tnt::HttpReply& reply,
		                                          tnt::QueryParams& qparam)
		{
			// set return header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get user ids
			std::list<int> ids;
			try
			{
				ids = models::dal::event_dal::load_groups_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// get user data
			std::list<models::group> gr;
			try
			{
				gr = models::dal::group_dal::load_by_ids(ids);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// write respone
			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(gr).finish();

			// return
			return HTTP_OK;
		}

		unsigned event_controller::details_users(tnt::HttpRequest& request,
		                                         tnt::HttpReply& reply,
		                                         tnt::QueryParams& qparam)
		{
			// set return header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get user ids
			std::list<int> ids;
			try
			{
				ids = models::dal::event_dal::load_users_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// get user data
			std::list<models::user> us;
			try
			{
				us = models::dal::user_dal::load_by_ids(ids);
			}
			catch(tea::error& e)
			{
				send_status(reply, e.get_http_code(), e.what(), true);
			}

			// write respone
			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(us).finish();

			// return
			return HTTP_OK;
		}

		unsigned event_controller::list(tnt::HttpRequest& request,
		                                tnt::HttpReply& reply,
		                                tnt::QueryParams& qparam)
		{
			// This method only exists so that it can be used in the future if
			// necesary.
			return DECLINED;
		}

		unsigned event_controller::list_data(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			std::list<models::event> ev;
			try
			{
				ev = models::dal::event_dal::load_all();
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(ev).finish();

			return HTTP_OK;
		}

		unsigned event_controller::remove(tnt::HttpRequest& request,
		                                  tnt::HttpReply& reply,
		                                  tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// delete event
			try
			{
				models::dal::event_dal::remove(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned event_controller::update(tnt::HttpRequest& request,
		                                  tnt::HttpReply& reply,
		                                  tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			// try to load
			models::event event;
			try
			{
				event = models::dal::event_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save event to request
			TNT_REQUEST_SHARED_VAR(tea::models::event, ev, ());
			ev = event;

			return DECLINED;
		}

		unsigned event_controller::update_detail_info(tnt::HttpRequest& request,
		                                              tnt::HttpReply& reply,
		                                              tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get description
			std::string desc
			    = get_string(reply, qparam, error, "description", true);
			if(error)
				return error;

			// get location
			std::string location
			    = get_string(reply, qparam, error, "location", true);
			if(error)
				return error;

			// get event
			models::event ev;
			try
			{
				ev = models::dal::event_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// update data
			ev.set_description(desc);
			ev.set_location(location);
			try
			{
				models::dal::event_dal::update(ev);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned event_controller::update_general(tnt::HttpRequest& request,
		                                          tnt::HttpReply& reply,
		                                          tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get all
			bool all = get_bool(reply, qparam, error, "all", true);
			if(error)
				return error;

			// get date
			int date_day = get_int(reply, qparam, error, "date-day", true);
			if(error)
				return error;
			int date_month = get_int(reply, qparam, error, "date-month", true);
			if(error)
				return error;
			int date_year = get_int(reply, qparam, error, "date-year", true);
			if(error)
				return error;
			try
			{
				check_date(date_year, date_month, date_day);
			}
			catch(std::exception& e)
			{
				return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
			}

			// get public
			bool publicity = get_bool(reply, qparam, error, "public", true);
			if(error)
				return error;

			// get time
			int time_hour = get_int(reply, qparam, error, "time-hour", true);
			if(error)
				return error;
			int time_min = get_int(reply, qparam, error, "time-min", true);
			if(error)
				return error;
			try
			{
				check_time(time_hour, time_min);
			}
			catch(std::exception& e)
			{
				return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
			}

			// get title
			std::string title = get_string(reply, qparam, error, "title", true);
			if(error)
				return error;

			// load event
			models::event ev;
			try
			{
				ev = models::dal::event_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			try
			{
				ev.set_title(title);
				ev.set_time(tntdb::Datetime(
				    date_year, date_month, date_day, time_hour, time_min, 0));
				ev.set_is_public(publicity);
				ev.set_is_for_all(all);
			}
			catch(std::exception& e)
			{
				return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
			}

			// store updated values
			try
			{
				models::dal::event_dal::update(ev);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned event_controller::update_group_add(tnt::HttpRequest& request,
		                                            tnt::HttpReply& reply,
		                                            tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			if(request.isMethodGET())
			{
				// get id
				int id = get_int(reply, qparam, error, "id", true);
				if(error)
					return error;

				// load groups
				std::list<int> ids;
				try
				{
					ids = models::dal::event_dal::load_groups_anti_by_id(id);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				// get group data
				std::list<models::group> groups;
				try
				{
					groups = models::dal::group_dal::load_by_ids(ids);
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}

				// write outpus
				cxxtools::JsonSerializer ser(reply.out());
				reply.out() << "{\"data\":";
				ser.serialize(groups).finish();
				reply.out() << "}";

				return HTTP_OK;
			}
			else
			{
				// get event id
				int id = get_int(reply, qparam, error, "id", true);
				if(error)
					return error;

				// get group id
				int group = get_int(reply, qparam, error, "group", true);
				if(error)
					return error;

				// add group to event
				try
				{
					models::dal::event_dal::add_group(id, group);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				return HTTP_NO_CONTENT;
			}
		}

		unsigned
		event_controller::update_group_remove(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get event id
			int ev_id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get group id
			int group_id = get_int(reply, qparam, error, "group", true);
			if(error)
				return error;

			// remove group from event
			try
			{
				models::dal::event_dal::remove_group(ev_id, group_id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned event_controller::update_user_add(tnt::HttpRequest& request,
		                                           tnt::HttpReply& reply,
		                                           tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			if(request.isMethodGET())
			{
				// get id
				int id = get_int(reply, qparam, error, "id", true);
				if(error)
					return error;

				// load users
				std::list<int> ids;
				try
				{
					ids = models::dal::event_dal::load_users_anti_by_id(id);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				// get user data
				std::list<models::user> users;
				try
				{
					users = models::dal::user_dal::load_by_ids(ids);
				}
				catch(tea::error& e)
				{
					send_status(reply, e.get_http_code(), e.what(), true);
				}

				// write output
				cxxtools::JsonSerializer ser(reply.out());
				reply.out() << "{\"data\":";
				ser.serialize(users).finish();
				reply.out() << "}";

				return HTTP_OK;
			}
			else
			{
				// get event id
				int id = get_int(reply, qparam, error, "id", true);
				if(error)
					return error;

				// get user id
				int user = get_int(reply, qparam, error, "user", true);
				if(error)
					return error;

				// add user to event
				try
				{
					models::dal::event_dal::add_user(id, user);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				return HTTP_NO_CONTENT;
			}
		}

		unsigned event_controller::update_user_remove(tnt::HttpRequest& request,
		                                              tnt::HttpReply& reply,
		                                              tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get event id
			int ev_id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get user id
			int us_id = get_int(reply, qparam, error, "user", true);
			if(error)
				return error;

			// remove user from event
			try
			{
				models::dal::event_dal::remove_user(ev_id, us_id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned event_controller::operator()(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			std::string path = request.getPathInfo();
			if(path == "add")
				return add(request, reply, qparam);
			else if(path == "details")
				return details(request, reply, qparam);
			else if(path == "details-groups")
				return details_groups(request, reply, qparam);
			else if(path == "details-users")
				return details_users(request, reply, qparam);
			else if(path == "list")
				return list(request, reply, qparam);
			else if(path == "list-data")
				return list_data(request, reply, qparam);
			else if(path == "remove")
				return remove(request, reply, qparam);
			else if(path == "update")
				return update(request, reply, qparam);
			else if(path == "update-detail-info")
				return update_detail_info(request, reply, qparam);
			else if(path == "update-general")
				return update_general(request, reply, qparam);
			else if(path == "update-group-add")
				return update_group_add(request, reply, qparam);
			else if(path == "update-group-remove")
				return update_group_remove(request, reply, qparam);
			else if(path == "update-user-add")
				return update_user_add(request, reply, qparam);
			else if(path == "update-user-remove")
				return update_user_remove(request, reply, qparam);
			else
				return send_status(reply, HTTP_NOT_FOUND);
		}
	} // namespace controllers
} // namespace tea
