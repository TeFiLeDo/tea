#include "user_controller.hpp"

#include "../convenience/error.hpp"
#include "../models/dal/event_dal.hpp"
#include "../models/dal/session_resume_dal.hpp"
#include "../models/dal/user_dal.hpp"

namespace tea
{
	namespace controllers
	{
		unsigned user_controller::add(tnt::HttpRequest& request,
		                              tnt::HttpReply& reply,
		                              tnt::QueryParams& qparam)
		{
			if(request.isMethodGET())
				return DECLINED;
			else
			{
				// set return type header
				reply.setHeader("Content-Type", "text/json; charset=UTF-8");

				// for error handling
				unsigned error;

				// get email
				std::string mail
				    = get_string(reply, qparam, error, "email", true);
				if(error)
					return error;

				// get first name
				std::string first_name = get_string(
				    reply, qparam, error, "firstname", true, false);
				if(error)
					return error;

				// get last name
				std::string last_name
				    = get_string(reply, qparam, error, "lastname", true, false);
				if(error)
					return error;

				// get admin
				bool admin = get_bool(reply, qparam, error, "admin", true);
				if(error)
					return error;

				// get locked out
				bool locked_out
				    = get_bool(reply, qparam, error, "locked", true);
				if(error)
					return error;

				models::user us;

				// try to create user object
				try
				{
					us = models::user(-1 /*ignored*/,
					                  mail,
					                  first_name,
					                  last_name,
					                  admin,
					                  locked_out);
				}
				catch(std::exception& e)
				{
					return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
				}

				// insert user into db
				try
				{
					models::dal::user_dal::add(us);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				// return
				return HTTP_NO_CONTENT;
			}
		}

		unsigned user_controller::details(tnt::HttpRequest& request,
		                                  tnt::HttpReply& reply,
		                                  tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			models::user us;

			// load user
			try
			{
				us = models::dal::user_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save data in request
			TNT_REQUEST_SHARED_VAR(tea::models::user, user_data, ());
			user_data = us;

			// return
			return DECLINED;
		}

		unsigned user_controller::list(tnt::HttpRequest& request,
		                               tnt::HttpReply& reply,
		                               tnt::QueryParams& qparam)
		{
			return DECLINED;
		}

		unsigned user_controller::list_data(tnt::HttpRequest& request,
		                                    tnt::HttpReply& reply,
		                                    tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			std::list<models::user> users;

			// get data
			try
			{
				users = models::dal::user_dal::load_all();
			}
			catch(error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(users).finish();

			return HTTP_OK;
		}

		unsigned user_controller::login(tnt::HttpRequest& request,
		                                tnt::HttpReply& reply,
		                                tnt::QueryParams& qparam)
		{
			if(request.isMethodGET())
			{
				return DECLINED;
			}
			else
			{
				// set return type header
				reply.setHeader("Content-Type", "text/json; charset=UTF-8");

				// for error handling
				unsigned error;

				// get email
				std::string email
				    = get_string(reply, qparam, error, "email", true);
				if(error)
					return error;

				// get password
				std::string password
				    = get_string(reply, qparam, error, "password", true);
				if(error)
					return error;

				models::user us;
				try
				{
					us = models::dal::user_dal::load_by_email_if_authenticated(
					    email, password);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());
				session_user = us;

				return HTTP_NO_CONTENT;
			}
		}

		unsigned user_controller::logout(tnt::HttpRequest& request,
		                                 tnt::HttpReply& reply,
		                                 tnt::QueryParams& qparam)
		{
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());
			session_user = models::user();

			if(request.hasCookie("lecasession"))
				models::dal::session_resume_dal::remove_by_id(
				    request.getCookie("lecasession"));

			return send_status(reply, HTTP_SEE_OTHER, "/default/index");
		}

		unsigned user_controller::remove(tnt::HttpRequest& request,
		                                 tnt::HttpReply& reply,
		                                 tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// remove from db
			try
			{
				models::dal::user_dal::remove(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// return
			return HTTP_NO_CONTENT;
		}

		unsigned user_controller::update(tnt::HttpRequest& request,
		                                 tnt::HttpReply& reply,
		                                 tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			models::user us;

			// load user
			try
			{
				us = models::dal::user_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save user to request
			TNT_REQUEST_SHARED_VAR(tea::models::user, user_data, ());
			user_data = us;

			return DECLINED;
		}

		unsigned user_controller::update_general(tnt::HttpRequest& request,
		                                         tnt::HttpReply& reply,
		                                         tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get first name
			std::string first_name
			    = get_string(reply, qparam, error, "firstname", true);
			if(error)
				return error;

			// get last name
			std::string last_name
			    = get_string(reply, qparam, error, "lastname", true);
			if(error)
				return error;

			// get locked
			bool locked = get_bool(reply, qparam, error, "locked", true);
			if(error)
				return error;

			// get rank
			bool rank = get_bool(reply, qparam, error, "rank", true);
			if(error)
				return error;

			// load user
			models::user us;
			try
			{
				us = models::dal::user_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// make changes
			try
			{
				us.set_first_name(first_name);
				us.set_last_name(last_name);
				us.set_is_admin(rank);
				us.set_is_locked_out(locked);
			}
			catch(std::exception& e)
			{
				return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
			}

			// try to save changes
			try
			{
				models::dal::user_dal::update(us);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// return
			return HTTP_NO_CONTENT;
		}

		unsigned user_controller::update_password(tnt::HttpRequest& request,
		                                          tnt::HttpReply& reply,
		                                          tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get password
			std::string password
			    = get_string(reply, qparam, error, "password", true);
			if(error)
				return error;

			// do update
			try
			{
				models::dal::user_dal::update_password(id, password);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			models::dal::session_resume_dal::remove_all_for_user(id);
			return HTTP_NO_CONTENT;
		}

		unsigned user_controller::operator()(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			std::string path = request.getPathInfo();

			if(path == "add")
				return add(request, reply, qparam);
			else if(path == "details")
				return details(request, reply, qparam);
			else if(path == "list")
				return list(request, reply, qparam);
			else if(path == "list-data")
				return list_data(request, reply, qparam);
			else if(path == "login")
				return login(request, reply, qparam);
			else if(path == "logout")
				return logout(request, reply, qparam);
			else if(path == "remove")
				return remove(request, reply, qparam);
			else if(path == "update")
				return update(request, reply, qparam);
			else if(path == "update-general")
				return update_general(request, reply, qparam);
			else if(path == "update-password")
				return update_password(request, reply, qparam);
			return DECLINED;
		}
	} // namespace controllers
} // namespace tea
