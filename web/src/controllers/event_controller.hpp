#pragma once

#include "../convenience/controllers.hpp"

namespace tea
{
	namespace controllers
	{
		class event_controller : public tnt::Component
		{
			log_define("tea.controller.event");

		private:
			unsigned add(tnt::HttpRequest& request,
			             tnt::HttpReply& reply,
			             tnt::QueryParams& qparam);
			unsigned details(tnt::HttpRequest& request,
			                 tnt::HttpReply& reply,
			                 tnt::QueryParams& qparam);
			unsigned details_groups(tnt::HttpRequest& request,
			                        tnt::HttpReply& reply,
			                        tnt::QueryParams& qparam);
			unsigned details_users(tnt::HttpRequest& request,
			                       tnt::HttpReply& reply,
			                       tnt::QueryParams& qparam);
			unsigned list(tnt::HttpRequest& request,
			              tnt::HttpReply& reply,
			              tnt::QueryParams& qparam);
			unsigned list_data(tnt::HttpRequest& request,
			                   tnt::HttpReply& reply,
			                   tnt::QueryParams& qparam);
			unsigned remove(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update_detail_info(tnt::HttpRequest& request,
			                            tnt::HttpReply& reply,
			                            tnt::QueryParams& qparam);
			unsigned update_general(tnt::HttpRequest& request,
			                        tnt::HttpReply& reply,
			                        tnt::QueryParams& qparam);
			unsigned update_group_add(tnt::HttpRequest& request,
			                          tnt::HttpReply& reply,
			                          tnt::QueryParams& qparam);
			unsigned update_group_remove(tnt::HttpRequest& request,
			                             tnt::HttpReply& reply,
			                             tnt::QueryParams& qparam);
			unsigned update_user_add(tnt::HttpRequest& request,
			                         tnt::HttpReply& reply,
			                         tnt::QueryParams& qparam);
			unsigned update_user_remove(tnt::HttpRequest& request,
			                            tnt::HttpReply& reply,
			                            tnt::QueryParams& qparam);

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<event_controller>
		    factory("tea/controller/event");
	} // namespace controllers
} // namespace tea
