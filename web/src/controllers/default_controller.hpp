#pragma once

#include "../convenience/controllers.hpp"

namespace tea
{
	namespace controllers
	{
		class default_controller : public tnt::Component
		{
			log_define("tea.controller.default");

		private:
			unsigned about(tnt::HttpRequest& request,
			               tnt::HttpReply& reply,
			               tnt::QueryParams& qparam);
			unsigned event_details(tnt::HttpRequest& request,
			                       tnt::HttpReply& reply,
			                       tnt::QueryParams& qparam);
			unsigned event_list(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
			unsigned event_list_data(tnt::HttpRequest& request,
			                         tnt::HttpReply& reply,
			                         tnt::QueryParams& qparam);
			unsigned imprint(tnt::HttpRequest& request,
			                 tnt::HttpReply& reply,
			                 tnt::QueryParams& qparam);
			unsigned index(tnt::HttpRequest& request,
			               tnt::HttpReply& reply,
			               tnt::QueryParams& qparam);
			unsigned index_list_data(tnt::HttpRequest& request,
			                         tnt::HttpReply& reply,
			                         tnt::QueryParams& qparam);
			unsigned settings(tnt::HttpRequest& request,
			                  tnt::HttpReply& reply,
			                  tnt::QueryParams& qparam);
			unsigned settings_password(tnt::HttpRequest& request,
			                           tnt::HttpReply& reply,
			                           tnt::QueryParams& qparam);
			unsigned settings_store_session(tnt::HttpRequest& request,
			                                tnt::HttpReply& reply,
			                                tnt::QueryParams& qparam);
			unsigned settings_username(tnt::HttpRequest& request,
			                           tnt::HttpReply& reply,
			                           tnt::QueryParams& qparam);

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<default_controller>
		    factory("tea/controller/default");
	} // namespace controllers
} // namespace tea
