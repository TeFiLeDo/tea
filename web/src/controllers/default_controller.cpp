#include "default_controller.hpp"

#include <fstream>

#include <cxxtools/jsondeserializer.h>

#include "../convenience/error.hpp"
#include "../models/dal/event_dal.hpp"
#include "../models/dal/group_dal.hpp"
#include "../models/dal/session_resume_dal.hpp"
#include "../models/dal/user_dal.hpp"
#include "../models/imprint.hpp"
#include "../models/user.hpp"

namespace tea
{
	namespace controllers
	{
		unsigned default_controller::about(tnt::HttpRequest& request,
		                                   tnt::HttpReply& reply,
		                                   tnt::QueryParams& qparam)
		{
			return DECLINED;
		}

		unsigned default_controller::event_details(tnt::HttpRequest& request,
		                                           tnt::HttpReply& reply,
		                                           tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			// get request var
			TNT_REQUEST_SHARED_VAR(tea::models::event, ev, ());

			// load data
			try
			{
				ev = models::dal::event_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// get session user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());
			log_debug(session_user.get_id());

			// check if user is allowed to see data
			if(ev.get_is_public()) // if is public, everyone may see it
				return DECLINED;

			if(session_user.get_id() >= 0) // only if user is logged in
			{
				// check if all are invited
				if(ev.get_is_for_all())
					return DECLINED;

				// check if user is invited
				try
				{
					auto evs = models::dal::user_dal::load_events_by_id(
					    session_user.get_id());
					if(std::find(evs.begin(), evs.end(), id) != evs.end())
						return DECLINED;
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}

				// check if user is in invited group
				try
				{
					auto u_gr = models::dal::user_dal::load_groups_by_id(
					    session_user.get_id());
					auto e_gr = models::dal::event_dal::load_groups_by_id(id);
					for(auto i = u_gr.begin(); i != u_gr.end(); i++)
						if(std::find(e_gr.begin(), e_gr.end(), *i)
						   != e_gr.end())
							return DECLINED;
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}
			}

			return send_status(reply, HTTP_FORBIDDEN);
		}

		unsigned default_controller::event_list(tnt::HttpRequest& request,
		                                        tnt::HttpReply& reply,
		                                        tnt::QueryParams& qparam)
		{
			return DECLINED;
		}

		unsigned default_controller::event_list_data(tnt::HttpRequest& request,
		                                             tnt::HttpReply& reply,
		                                             tnt::QueryParams& qparam)
		{
			// set return header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// check if passed events should be loaded
			bool include_passed
			    = get_bool(reply, qparam, error, "passed", true, true);
			if(error)
				return error;

			// get session user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			// get events
			std::list<models::event> evs;
			// events user is invited to
			std::list<models::event> u_evs;
			try
			{
				u_evs = models::dal::event_dal::load_by_ids(
				    models::dal::user_dal::load_events_by_id(
				        session_user.get_id()));
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}
			evs.insert(evs.end(), u_evs.begin(), u_evs.end());
			// events groups are invited to
			std::list<models::event> g_evs;
			try
			{
				std::list<int> groups
				    = models::dal::user_dal::load_groups_by_id(
				        session_user.get_id());
				for(auto i = groups.begin(); i != groups.end(); i++)
				{
					std::list<models::event> l_evs
					    = models::dal::event_dal::load_by_ids(
					        models::dal::group_dal::load_events_by_id(*i));
					g_evs.insert(g_evs.end(), l_evs.begin(), l_evs.end());
				}
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}
			evs.insert(evs.end(), g_evs.begin(), g_evs.end());
			// events all are invited to
			std::list<models::event> a_evs;
			try
			{
				a_evs = models::dal::event_dal::load_all();
				for(auto i = a_evs.begin(); i != a_evs.end(); i++)
					if(i->get_is_for_all())
						evs.push_back(*i);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// make entries in list unique
			std::map<int, models::event> m_evs;
			for(auto i = evs.begin(); i != evs.end(); i++)
				m_evs[i->get_id()] = *i;
			evs.clear();
			for(auto i = m_evs.begin(); i != m_evs.end(); i++)
				evs.push_back(i->second);

			// filter events
			if(!include_passed)
			{
				auto i = evs.begin();
				while(i != evs.end())
				{
					if(i->get_time() < tntdb::Datetime::localtime())
						i = evs.erase(i);
					else
						i++;
				}
			}

			// write return
			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(evs).finish();

			return HTTP_OK;
		}

		unsigned default_controller::imprint(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			// load data
			models::imprint imp;
			try
			{
				std::ifstream f("imprint.json");
				cxxtools::JsonDeserializer ser(f);
				ser.deserialize(imp);
			}
			catch(std::exception& e)
			{
				log_error(
				    std::string("Failed to load imprint config. Message: ")
				    + e.what());
				return send_status(reply, HTTP_INTERNAL_SERVER_ERROR);
			}

			TNT_REQUEST_SHARED_VAR(tea::models::imprint, im, ());
			im = imp;

			return DECLINED;
		}

		unsigned default_controller::index(tnt::HttpRequest& request,
		                                   tnt::HttpReply& reply,
		                                   tnt::QueryParams& qparam)
		{
			return DECLINED;
		}

		unsigned default_controller::index_list_data(tnt::HttpRequest& request,
		                                             tnt::HttpReply& reply,
		                                             tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			std::list<models::event> evs;
			try
			{
				evs = models::dal::event_dal::load_all_of_actual_month();
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(evs).finish();
			return HTTP_OK;
		}

		unsigned default_controller::settings(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			// get user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			// write user name to request
			TNT_REQUEST_SHARED_VAR(std::string, first_name, ());
			first_name = session_user.get_first_name();
			TNT_REQUEST_SHARED_VAR(std::string, last_name, ());
			last_name = session_user.get_last_name();

			return DECLINED;
		}

		unsigned
		default_controller::settings_password(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// get user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			// for error handling
			unsigned error;

			// get sent password (old)
			std::string pw_old = get_string(reply, qparam, error, "pwo", true);
			if(error)
				return error;

			// get sent password (new)
			std::string pw_new = get_string(reply, qparam, error, "pwn", true);
			if(error)
				return error;

			// check if old password is correct
			try
			{
				models::dal::user_dal::load_by_email_if_authenticated(
				    session_user.get_email(), pw_old);
			}
			catch(const std::exception& e)
			{
				return send_status(reply,
				                   HTTP_BAD_REQUEST,
				                   "Das momentane Passwort ist nicht korrekt.",
				                   true);
			}

			// update password
			models::dal::user_dal::update_password(session_user.get_id(),
			                                       pw_new);

			models::dal::session_resume_dal::remove_all_for_user(
			    session_user.get_id());

			// return
			return HTTP_NO_CONTENT;
		}
		unsigned
		default_controller::settings_store_session(tnt::HttpRequest& request,
		                                           tnt::HttpReply& reply,
		                                           tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// get user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			std::string id;

			try
			{
				id = models::dal::session_resume_dal::add(
				    session_user.get_id());
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			reply.setCookie("lecasession",
			                tnt::Cookie(id, 2592000)
			                    .setSecure(false)
			                    .setComment("HttpOnly"));
#warning Session resume cookie isnt secure

			return 204;
		}

		unsigned
		default_controller::settings_username(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// get user
			TNT_SESSION_SHARED_VAR(tea::models::user, session_user, ());

			// for error handling
			unsigned error;

			// get new first name
			std::string first_name
			    = get_string(reply, qparam, error, "firstname", true);
			if(error)
				return error;

			// get new last name
			std::string last_name
			    = get_string(reply, qparam, error, "lastname", true);
			if(error)
				return error;

			// update user object
			session_user.set_first_name(first_name);
			session_user.set_last_name(last_name);

			// update db
			try
			{
				models::dal::user_dal::update(session_user);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// return
			return HTTP_NO_CONTENT;
		}

		unsigned default_controller::operator()(tnt::HttpRequest& request,
		                                        tnt::HttpReply& reply,
		                                        tnt::QueryParams& qparam)
		{
			std::string path = request.getPathInfo();
			if(path == "about")
				return about(request, reply, qparam);
			else if(path == "event-details")
				return event_details(request, reply, qparam);
			else if(path == "event-list")
				return event_list(request, reply, qparam);
			else if(path == "event-list-data")
				return event_list_data(request, reply, qparam);
			else if(path == "imprint")
				return imprint(request, reply, qparam);
			else if(path == "index")
				return index(request, reply, qparam);
			else if(path == "index-list-data")
				return index_list_data(request, reply, qparam);
			else if(path == "settings")
				return settings(request, reply, qparam);
			else if(path == "settings-password")
				return settings_password(request, reply, qparam);
			else if(path == "settings-store-session")
				return settings_store_session(request, reply, qparam);
			else if(path == "settings-username")
				return settings_username(request, reply, qparam);
			else
				return send_status(reply, HTTP_NOT_FOUND);
		}
	} // namespace controllers
} // namespace tea
