#include "group_controller.hpp"

#include "../convenience/error.hpp"
#include "../models/dal/group_dal.hpp"
#include "../models/dal/user_dal.hpp"
#include "../models/group.hpp"

namespace tea
{
	namespace controllers
	{
		unsigned group_controller::add(tnt::HttpRequest& request,
		                               tnt::HttpReply& reply,
		                               tnt::QueryParams& qparam)
		{
			if(request.isMethodGET())
			{
				// load all users
				std::list<models::user> users;

				try
				{
					users = models::dal::user_dal::load_all();
				}
				catch(tea::error& e)
				{
					return send_status(reply, e.get_http_code());
				}

				// save all users to request
				TNT_REQUEST_SHARED_VAR(
				    std::list<tea::models::user>, us_data, ());
				us_data = users;

				return DECLINED;
			}
			else
			{
				// set return type header
				reply.setHeader("Content-Type", "text/json; charset=UTF-8");

				// for error handling
				unsigned error;

				// get ids from invited users
				std::string user_ids_string
				    = get_string(reply, qparam, error, "users", true);
				if(error)
					return error;
				std::list<int> user_ids = split_int_list(user_ids_string);

				// try to create model (will throw exception if name is
				// invalid)
				models::group gr;
				try
				{
					gr = models::group(-1, qparam.param("name"));
				}
				catch(std::exception& e)
				{
					return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
				}

				try
				{
					models::dal::group_dal::add(gr, user_ids);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				return HTTP_NO_CONTENT;
			}
		}

		unsigned group_controller::details(tnt::HttpRequest& request,
		                                   tnt::HttpReply& reply,
		                                   tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id");
			if(error)
				return error;

			// load data
			models::group gr;
			try
			{
				gr = models::dal::group_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save data in request
			TNT_REQUEST_SHARED_VAR(tea::models::group, group_data, ());
			group_data = gr;

			// return DECLINED to proceed.
			return DECLINED;
		}

		unsigned group_controller::details_user_list(tnt::HttpRequest& request,
		                                             tnt::HttpReply& reply,
		                                             tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int group_id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get user ids
			std::list<int> user_ids;
			try
			{
				user_ids = models::dal::group_dal::load_users_by_id(group_id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// get users
			std::list<models::user> users;
			try
			{
				users = models::dal::user_dal::load_by_ids(user_ids);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// write return
			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(users).finish();

			return 200;
		}

		unsigned group_controller::list(tnt::HttpRequest& request,
		                                tnt::HttpReply& reply,
		                                tnt::QueryParams& qparam)
		{
			// this method only exists because there could be some use for it in
			// the future.
			return DECLINED;
		}

		unsigned group_controller::list_data(tnt::HttpRequest& request,
		                                     tnt::HttpReply& reply,
		                                     tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// get data from dal
			std::list<models::group> groups;
			try
			{
				groups = models::dal::group_dal::load_all();
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// write to stream
			cxxtools::JsonSerializer ser(reply.out());
			ser.serialize(groups).finish();

			return HTTP_OK;
		}

		unsigned group_controller::remove(tnt::HttpRequest& request,
		                                  tnt::HttpReply& reply,
		                                  tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error = 0;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error != 0)
				return error;

			// delete group
			try
			{
				models::dal::group_dal::remove(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned group_controller::update(tnt::HttpRequest& request,
		                                  tnt::HttpReply& reply,
		                                  tnt::QueryParams& qparam)
		{
			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", false);
			if(error != 0)
				return error;

			// try to load
			models::group group;
			try
			{
				group = models::dal::group_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code());
			}

			// save group to request
			TNT_REQUEST_SHARED_VAR(tea::models::group, gr, ());
			gr = group;

			return DECLINED;
		}

		unsigned group_controller::update_general(tnt::HttpRequest& request,
		                                          tnt::HttpReply& reply,
		                                          tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int id = get_int(reply, qparam, error, "id", true);
			if(error != 0)
				return error;

			// get new name
			std::string name
			    = get_string(reply, qparam, error, "name", true, true);
			if(error != 0)
				return error;

			// load group
			models::group gr;
			try
			{
				gr = models::dal::group_dal::load_by_id(id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			// make changes
			try
			{
				gr.set_name(name);
			}
			catch(std::exception& e)
			{
				return send_status(reply, HTTP_BAD_REQUEST, e.what(), true);
			}

			// save changes
			try
			{
				models::dal::group_dal::update(gr);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}

			return HTTP_NO_CONTENT;
		}

		unsigned group_controller::update_user_add(tnt::HttpRequest& request,
		                                           tnt::HttpReply& reply,
		                                           tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			if(request.isMethodGET())
			{
				// get group id
				int id = get_int(reply, qparam, error, "id", true, false);
				if(error)
					return error;

				// get users
				std::list<int> user_ids;
				std::list<models::user> users;
				try
				{
					user_ids
					    = models::dal::group_dal::load_users_anti_by_id(id);
					users = models::dal::user_dal::load_by_ids(user_ids);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				// write results to client
				reply.out() << "{\"data\":";
				cxxtools::JsonSerializer ser(reply.out());
				ser.serialize(users).finish();
				reply.out() << "}";

				// return
				return HTTP_OK;
			}
			else
			{
				// get group id
				int group_id = get_int(reply, qparam, error, "id", true);
				if(error)
					return error;

				// get user id
				int user_id = get_int(reply, qparam, error, "user", true);
				if(error)
					return error;

				// add user to group
				try
				{
					models::dal::group_dal::add_user(group_id, user_id);
				}
				catch(tea::error& e)
				{
					return send_status(
					    reply, e.get_http_code(), e.what(), true);
				}

				// return
				return HTTP_NO_CONTENT;
			}
		}

		unsigned group_controller::update_user_remove(tnt::HttpRequest& request,
		                                              tnt::HttpReply& reply,
		                                              tnt::QueryParams& qparam)
		{
			// set return type header
			reply.setHeader("Content-Type", "text/json; charset=UTF-8");

			// for error handling
			unsigned error;

			// get id
			int group_id = get_int(reply, qparam, error, "id", true);
			if(error)
				return error;

			// get user
			int user_id = get_int(reply, qparam, error, "user", true);
			if(error)
				return error;

			try
			{
				models::dal::group_dal::remove_user(group_id, user_id);
			}
			catch(tea::error& e)
			{
				return send_status(reply, e.get_http_code(), e.what(), true);
			}
			return HTTP_NO_CONTENT;
		}

		unsigned group_controller::operator()(tnt::HttpRequest& request,
		                                      tnt::HttpReply& reply,
		                                      tnt::QueryParams& qparam)
		{
			std::string path = request.getPathInfo();
			if(path == "add")
				return add(request, reply, qparam);
			else if(path == "details")
				return details(request, reply, qparam);
			else if(path == "details-user-list")
				return details_user_list(request, reply, qparam);
			else if(path == "list")
				return list(request, reply, qparam);
			else if(path == "list-data")
				return list_data(request, reply, qparam);
			else if(path == "remove")
				return remove(request, reply, qparam);
			else if(path == "update")
				return update(request, reply, qparam);
			else if(path == "update-general")
				return update_general(request, reply, qparam);
			else if(path == "update-user-add")
				return update_user_add(request, reply, qparam);
			else if(path == "update-user-remove")
				return update_user_remove(request, reply, qparam);
			else
				return send_status(reply, HTTP_NOT_FOUND);
		}
	} // namespace controllers
} // namespace tea
