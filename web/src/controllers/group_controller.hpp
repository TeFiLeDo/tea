#pragma once

#include "../convenience/controllers.hpp"

namespace tea
{
	namespace controllers
	{
		class group_controller : public tnt::Component
		{
			log_define("tea.controller.group");

		private:
			unsigned add(tnt::HttpRequest& request,
			             tnt::HttpReply& reply,
			             tnt::QueryParams& qparam);
			unsigned details(tnt::HttpRequest& request,
			                 tnt::HttpReply& reply,
			                 tnt::QueryParams& qparam);
			unsigned details_user_list(tnt::HttpRequest& request,
			                           tnt::HttpReply& reply,
			                           tnt::QueryParams& qparam);
			unsigned list(tnt::HttpRequest& request,
			              tnt::HttpReply& reply,
			              tnt::QueryParams& qparam);
			unsigned list_data(tnt::HttpRequest& request,
			                   tnt::HttpReply& reply,
			                   tnt::QueryParams& qparam);
			unsigned remove(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update_general(tnt::HttpRequest& request,
			                        tnt::HttpReply& reply,
			                        tnt::QueryParams& qparam);
			unsigned update_user_add(tnt::HttpRequest& request,
			                         tnt::HttpReply& reply,
			                         tnt::QueryParams& qparam);
			unsigned update_user_remove(tnt::HttpRequest& request,
			                            tnt::HttpReply& reply,
			                            tnt::QueryParams& qparam);

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<group_controller>
		    factory("tea/controller/group");
	} // namespace controllers
} // namespace tea
