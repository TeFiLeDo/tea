#pragma once

#include "../convenience/controllers.hpp"
#include "../models/user.hpp"

namespace tea
{
	namespace controllers
	{
		class user_controller : public tnt::Component
		{
			log_define("tea.controller.user");

		private:
			unsigned add(tnt::HttpRequest& request,
			             tnt::HttpReply& reply,
			             tnt::QueryParams& qparam);
			unsigned details(tnt::HttpRequest& request,
			                 tnt::HttpReply& reply,
			                 tnt::QueryParams& qparam);
			unsigned list(tnt::HttpRequest& request,
			              tnt::HttpReply& reply,
			              tnt::QueryParams& qparam);
			unsigned list_data(tnt::HttpRequest& request,
			                   tnt::HttpReply& reply,
			                   tnt::QueryParams& qparam);
			unsigned login(tnt::HttpRequest& request,
			               tnt::HttpReply& reply,
			               tnt::QueryParams& qparam);
			unsigned logout(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned remove(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update(tnt::HttpRequest& request,
			                tnt::HttpReply& reply,
			                tnt::QueryParams& qparam);
			unsigned update_general(tnt::HttpRequest& request,
			                        tnt::HttpReply& reply,
			                        tnt::QueryParams& qparam);
			unsigned update_password(tnt::HttpRequest& request,
			                         tnt::HttpReply& reply,
			                         tnt::QueryParams& qparam);

		public:
			unsigned operator()(tnt::HttpRequest& request,
			                    tnt::HttpReply& reply,
			                    tnt::QueryParams& qparam);
		};

		static tnt::ComponentFactoryImpl<user_controller>
		    factory("tea/controller/user");
	} // namespace controllers
} // namespace tea
