#pragma once

#include <stdexcept>

namespace tea
{
	class error : public std::runtime_error
	{
	private:
		unsigned http_code;

	public:
		error(unsigned http_code = 500, std::string msg = "error");
		unsigned get_http_code();
	};
}; // namespace tea
