#include "error.hpp"

namespace tea
{
	error::error(unsigned http_code, std::string msg)
	    : http_code(http_code), std::runtime_error(msg)
	{}

	unsigned error::get_http_code()
	{
		return http_code;
	}
} // namespace tea
