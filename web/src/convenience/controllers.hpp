#pragma once

#include <cxxtools/jsonserializer.h>

#include <tnt/component.h>
#include <tnt/componentfactory.h>
#include <tnt/httpreply.h>
#include <tnt/httprequest.h>

namespace tea
{
	namespace controllers
	{
		void check_date(int year, int month, int day);
		void check_time(int hour, int minute);
		bool get_bool(tnt::HttpReply& reply,
		              tnt::QueryParams& qparam,
		              unsigned& error,
		              std::string param_name,
		              bool is_json = false,
		              bool is_optional = false,
		              bool default_value = false);
		int get_int(tnt::HttpReply& reply,
		            tnt::QueryParams& qparam,
		            unsigned& error,
		            std::string param_name,
		            bool is_json = false,
		            bool is_optional = false);
		std::string get_string(tnt::HttpReply& reply,
		                       tnt::QueryParams& qparam,
		                       unsigned& error,
		                       std::string param_name,
		                       bool is_json = false,
		                       bool is_optional = false);
		unsigned send_status(tnt::HttpReply& reply,
		                     unsigned status_code,
		                     std::string param = "",
		                     bool isJson = false);
		std::list<int> split_int_list(std::string val);
	} // namespace controllers
} // namespace tea
