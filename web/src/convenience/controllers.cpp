#include "controllers.hpp"

namespace tea
{
	namespace controllers
	{
		void check_date(int year, int month, int day)
		{
			// check if year is in supported range
			if(year < 1990 || year >= 2100)
				throw std::runtime_error(
				    "Der unterstützte Zeitrahmen reicht nur vom 1. Januar 1990 "
				    "bis zum 31. Dezember 2000.");

			// check if month is correct
			if(month < 1 || month > 12)
				throw std::runtime_error(
				    "Es werden nur gültige Monate (1-12) akzeptiert.");

			// check day - 0 or negative
			if(day <= 0)
				throw std::runtime_error(
				    "Kein Monat hat nicht mindestens einen ersten Tag.");

			// check day - 31-day-months
			if(month == 1 || month == 3 || month == 5 || month == 7
			   || month == 8 || month == 10 || month == 12)
			{
				if(day > 31)
					throw std::runtime_error(
					    "Der ausgewählte Monat hat nur 31 Tage.");
				else
					return;
			}

			// check day - 30-day-months
			if(month == 4 || month == 6 || month == 9 || month == 11)
			{
				if(day > 30)
					throw std::runtime_error(
					    "Der ausgewählte Monat hat nur 30 Tage.");
				else
					return;
			}

			// check day - february
			if(day <= 28)
				return;

			if(year % 400 == 0 && day == 29)
				return;

			if(year % 100 == 0 && day == 29)
				throw std::runtime_error(
				    "Im ersten Jahr eines Jahrhunderts hat der Februar keine "
				    "Schalttag, außer das Jahr ist durch 400 teilbar.");

			if(year % 4 == 0 && day == 29)
				return;

			throw std::runtime_error(
			    "Der ausgewählte Monat hat nur 28 bis 29 Tage (googlen sie für "
			    "weitere Informationen bitte nach 'Schaltjahr'.");
		}

		void check_time(int hour, int min)
		{
			if(hour < 0 || hour > 23)
				throw std::runtime_error(
				    "Ein Tag hat 24 Stunden, diese beginnen mit 0 und enden "
				    "mit 23.");
			else if(min < 0 || min > 59)
				throw std::runtime_error(
				    "Eine Stunde hat 60 Minuten. Diese beginnen mit 0 und "
				    "enden mit 59.");
		}

		bool get_bool(tnt::HttpReply& reply,
		              tnt::QueryParams& qparam,
		              unsigned& error,
		              std::string param_name,
		              bool is_json,
		              bool is_optional,
		              bool default_value)
		{
			// set error to 0 so no former error affects result
			error = 0;

			// if no param_name was specified
			if(param_name == "")
			{
				error = send_status(reply,
				                    HTTP_INTERNAL_SERVER_ERROR,
				                    "Es wird versucht einen namenlosen "
				                    "boolschen Wert auszulesen.",
				                    is_json);
				return false;
			}

			// if param is absent
			if(qparam.param(param_name).length() == 0 && !is_optional)
			{
				error = send_status(reply,
				                    HTTP_UNPROCESSABLE_ENTITY,
				                    "Der Parameter '" + param_name
				                        + "' muss angegeben werden.",
				                    is_json);
				return false;
			}

			// get param
			std::string par = qparam.param(param_name);
			if(par == "true")
				return true;
			else if(par == "false")
				return false;
			else if(is_optional)
				return default_value;

			// else
			error = send_status(
			    reply,
			    HTTP_BAD_REQUEST,
			    "Der Parameter '" + param_name
			        + "' muss entwerder 'true' oder 'false' lauten.",
			    is_json);
			return false;
		}

		int get_int(tnt::HttpReply& reply,
		            tnt::QueryParams& qparam,
		            unsigned& error,
		            std::string param_name,
		            bool is_json,
		            bool is_optional)
		{
			// set error to 0 so no former error affects result
			error = 0;

			// if no param_name was specified
			if(param_name == "")
			{
				error = send_status(
				    reply,
				    HTTP_INTERNAL_SERVER_ERROR,
				    "Es wird versucht eine namenlose Zahl auszulesen.",
				    is_json);
				return 0;
			}

			// if param is absent
			if(qparam.param(param_name).length() == 0 && !is_optional)
			{
				error = send_status(reply,
				                    HTTP_UNPROCESSABLE_ENTITY,
				                    "Der Parameter '" + param_name
				                        + "' muss angegeben werden.",
				                    is_json);
				return 0;
			}

			// try to convert param to number
			int ret = 0;
			try
			{
				ret = std::stoi(qparam.param(param_name));
			}
			catch(...)
			{
				error = send_status(reply,
				                    HTTP_BAD_REQUEST,
				                    "Der Parameter '" + param_name
				                        + "' muss eine Ganzzahl sein.",
				                    is_json);
			}

			// return result
			return ret;
		}

		std::string get_string(tnt::HttpReply& reply,
		                       tnt::QueryParams& qparam,
		                       unsigned& error,
		                       std::string param_name,
		                       bool is_json,
		                       bool is_optional)
		{
			// set error to 0
			error = 0;

			// handle missing parameter name
			if(param_name == "")
			{
				error = send_status(
				    reply,
				    HTTP_INTERNAL_SERVER_ERROR,
				    "Es wird versucht eine namenlose Zeichenkette auszulesen.",
				    is_json);
				return "";
			}

			// if param is absent
			if(!qparam.has(param_name) && !is_optional)
			{
				error = send_status(reply,
				                    HTTP_UNPROCESSABLE_ENTITY,
				                    "Der Parameter \"" + param_name
				                        + "\" muss angegeben werden.",
				                    is_json);
				return "";
			}

			// return param
			return qparam.param(param_name);
		}

		unsigned send_status(tnt::HttpReply& reply,
		                     unsigned status_code,
		                     std::string param,
		                     bool isJson)
		{
			// clang-format off
			std::string top
			    = "<!DOCTYPE html>\r\n"
			      "<html>\r\n"
			          "<head>\r\n"
			              "<title>TEA - Fehler</title>\r\n"
			              "<meta charset=\"UTF-8\" />\r\n"
			              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\r\n"
			              "<link rel=\"stylesheet\" href=\"/static/semantic-ui-css/semantic.css\" />\r\n"
			              "<script src=\"/static/jquery/jquery.min.js\"></script>\r\n"
			              "<script src=\"/static/semantic-ui-css/semantic.js\"></script>\r\n"
			          "</head>\r\n"
			          "<body>\r\n"
			              "<div class=\"ui basic segment container\" style=\"height: 100%\">\r\n"
			                  "<div class=\"ui centered middle aligned doubling grid\" style=\"height: 100%\">\r\n"
			                      "<div class=\"sixteen wide mobile eight wide tablet six wide computer column\">\r\n"
			                          "<h1 class=\"ui top attached red header\">\r\n"
			                              "<i class=\"exclamation triangle icon\"></i>\r\n"
			                              "<div class=\"content\">\r\n";
			std::string middle
			    =                             "<div class=\"sub header\">Ein Fehler ist aufgetreten!</div>\r\n"
			                              "</div>\r\n"
			                          "</h1>\r\n"
			                          "<div class=\"ui attached segment\">\r\n";
			std::string bottom
			    =                     "</div>\r\n"
			                          "<a class=\"ui bottom attached blue button\" href=\"/\">Zurück zur Startseite</a>\r\n"
			                      "</div>\r\n"
			                  "</div>\r\n"
			              "</div>\r\n"
			          "</body>\r\n"
			      "</html>\r\n";
			// clang-format on

			std::string header, message;
			switch(status_code)
			{
				// 3xx
			case 303: // see other
				header = "drei null drei";
				message = "Die eigentlich gesuchte Ressource ist <a href=\""
				    + param + "\">hier</a> zu finden.";
				break;
			case 307: // temporary redirect
				header = "drei null sieben";
				message
				    = "Die angeforderte Ressource steht vorübergehend <a "
				      "href=\""
				    + param + "\">hier</a> zur Verfügung.";
				break;
			case 308: // permanent redirec
				header = "drei null acht";
				message
				    = "Die angeforderte Ressource steht in Zukunt <a href=\""
				    + param + "\">hier</a> zur Verfügung.";
				break;

				// 4xx
			case 400: // bad request
				header = "vier null null";
				message = "Die Anfrage an den Server war fehlerhaft.";
				break;
			case 403: // forbidden
				header = "vier null drei";
				message
				    = "Der Zugriff wurde aufgrund von unzureichenden "
				      "Benutzerrechten nicht gestattet. (Eventuell musst du "
				      "dich "
				      "einfach erneut anmelden.)";
				break;
			case 404: // not found
				header = "vier null vier";
				message
				    = "Die angeforderte Ressource konnte nicht gefunden "
				      "werden.";
				break;
			case 405: // method not allowed
				header = "vier null fünf";
				message
				    = "Die angeforderte Ressource steht unter dieser "
				      "HTTP-Methode nicht zur Verfügung.";
				break;
			case 409: // conflict
				header = "vier null neun";
				message
				    = "Die Operation konnte wegen eines Konflikts nicht "
				      "durchgeführt werden.";
				break;
			case 410: // gone
				header = "vier eins null";
				message
				    = "Die angeforderte Ressource steht nicht mehr zur "
				      "Verfügung";
				break;
			case 422: // unprocessable entity
				header = "vier zwei zwei";
				message
				    = "Die Anfrage konnte nicht verarbeitet werden, da ihr "
				      "Inhalt keinen Sinn macht.";
				break;

				// 5xx
				// 500 - internal server errror is handled with the default
				// section
			case 501: // not implemented
				header = "fünf null eins";
				message = "Diese Funktion steht noch nicht zur Verfügung.";
				break;
			case 503: // service unavailable
				header = "fünf null drei";
				message
				    = "Die angefragte Dienstleistung steht derzeit nicht zur "
				      "Verfügung.";
				break;

				// default
			default: // = 500 - internal server error
				status_code = 500;
				header = "fünf null null";
				message
				    = "Der Server konnte kein Ergebniss für die Anfrage "
				      "erarbeiten.";
				break;
			}

			// set headers
			if(status_code == 303 || status_code == 307 || status_code == 308)
				reply.setHeader("Location", param);
			else if(status_code == 405)
				reply.setHeader("Allow", param);

			if(status_code == 303 && isJson)
			{
				reply.out() << "{\"location\": \"" + param + "\"}";
				return 200;
			}
			else if(isJson)
				reply.out() << "{\"message\": \"" + param + "\"}";
			else
				reply.out() << top << header << "\r\n"
				            << middle << message << "\r\n"
				            << bottom;

			return status_code;
		}

		std::list<int> split_int_list(std::string val)
		{
			std::list<int> vals;
			if(val.length() == 0)
				return vals;

			std::istringstream ss(val);
			std::string s;
			while(std::getline(ss, s, ','))
			{
				try
				{
					vals.push_back(std::stoi(s));
				}
				catch(...)
				{
					continue;
				}
			}

			return vals;
		}
	} // namespace controllers
} // namespace tea
