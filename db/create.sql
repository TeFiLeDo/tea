CREATE DATABASE IF NOT EXISTS tea;
USE tea;

-- drop all tables

DROP TABLE IF EXISTS groupevent;
DROP TABLE IF EXISTS userevent;
DROP TABLE IF EXISTS usergroup;
DROP TABLE IF EXISTS sessionResume;
DROP TABLE IF EXISTS events;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS users;

-- create data tables

CREATE TABLE users (
	          id INT          NOT NULL AUTO_INCREMENT,
	       email VARCHAR(256) NOT NULL UNIQUE,
	   firstName VARCHAR(30),
	    lastName VARCHAR(30),
	    password VARCHAR(256) NOT NULL,
	passwordSalt VARCHAR(256) NOT NULL,
	     isAdmin BOOL         NOT NULL DEFAULT false,
	 isLockedOut BOOL         NOT NULL DEFAULT false,

	PRIMARY KEY(id)
);

CREATE TABLE groups (
	  id INT         NOT NULL AUTO_INCREMENT,
	name VARCHAR(30) NOT NULL UNIQUE,

	PRIMARY KEY(id)
);

CREATE TABLE events (
	         id INT         NOT NULL AUTO_INCREMENT,
	      title VARCHAR(30) NOT NULL,
	description TEXT,
	       time DATETIME    NOT NULL DEFAULT NOW(),
	   location TEXT,
	   isPublic BOOL        NOT NULL DEFAULT false,
	   isForAll BOOL        NOT NULL DEFAULT false,

	PRIMARY KEY(id),
	UNIQUE KEY (title, time)
);

-- session resume

CREATE TABLE sessionResume (
	      userId INT          NOT NULL,
	  identifier VARCHAR(256) NOT NULL,
	passwordHash VARCHAR(256) NOT NULL,
	    creation DATETIME     NOT NULL DEFAULT NOW(),

	PRIMARY KEY(userId, identifier)
);

-- create event connection tables

CREATE TABLE usergroup (
	 userId INT NOT NULL,
	groupId INT NOT NULL,

	PRIMARY KEY(userId, groupId),
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (groupId) REFERENCES groups(id) ON DELETE CASCADE
);

CREATE TABLE userevent (
	 userId INT NOT NULL,
	eventId INT NOT NULL,

	PRIMARY KEY(userId, eventId),
	FOREIGN KEY (userId) REFERENCES users(id) ON DELETE CASCADE,
	FOREIGN KEY (eventId) REFERENCES events(id) ON DELETE CASCADE
);

CREATE TABLE groupevent (
	groupId INT NOT NULL,
	eventId INT NOT NULL,

	PRIMARY KEY (groupId, eventId),
	FOREIGN KEY (groupId) REFERENCES groups(id) ON DELETE CASCADE,
	FOREIGN KEY (eventId) REFERENCES events(id) ON DELETE CASCADE
);
