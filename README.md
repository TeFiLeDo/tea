TEA
===

What is TEA?
------------

TEA is a web based application that allows groups of people to organize their
shared events effectively.

What features does TEA provide?
-------------------------------

- **web based interface**: Anyone with a browser can browse to the site. It is
  responsive, so you can use it on your phone too.
- **user and group system**: You can create new user accounts for the members
  of your organization. You also can add them to groups to manage bigger amoutns
  of members effectively.
- **public event list**: You can deceide whether an event should be public or
  not. Public events of the current month are displayed on the start page for
  all to see.

Prerequisites
-------------

1. A running mariadb server. Theoretically PostgreSQL and Oracle should work too,
   but that isn't tested.
2. You need to install cxxtools: https://github.com/maekitalo/cxxtools
3. You need to install tntnet: https://github.com/maekitalo/tntnet
4. You need to install tntdb: https://github.com/maekitalo/tntdb
   Make sure that you install a version with support for your database.
5. Make sure you've got CMake and a CPP-compiler installed.

Installation
------------

1. Make sure you've got all the prerequisites.
2. Clone this repository.
3. Go to the newly cloned direcotry.
4. Use the script _db/create.sql_ to create the database for TEA.
5. Change to the _web/_ folder.
6. Create a new folder called _build/_.
7. Change to the _build/_ folder.
8. Run `cmake ..`.
9. Run `make`.

Configuration
-------------

TEA needs 3 configuration files in the directory it is executed from:
- imprint.json
- log.json
- tea.json

Create the imprint.json file with this content:
```{
        "name": "The name of your organization.",
        "emails": [
                "your@email.com"
        ],
        "phones": [
                "+xx your number"
        ],
        "address": [
			"your addess",
			"in as many lines as you need",
			"just add new entries to this",
			"array"
        ],
        "zvr": "123456789",
        "opening": [
                "Your _\"Offenlegung\"_"
        ]
}
```

Create the log.json with this content:
```{
        "rootlogger": "WARNING",
        "loggers": [
                {
                        "category": "tea",
                        "level": "INFO"
                }
        ],
        "file": "log.txt"
}
```

Create the tea.json with this content:
```{
        "accessLog": "access.txt",
        "server": "tea1.0",
        "listeners": [
                {
                        "port": 8000
                }
        ],
        "environment": {
                "dbString": "mysql:dbname=tea;user=root"
        },
		"startpageEmoticon": "smile outline",
		"startpageText": "Der Text, der auf der Startseite angezeigt wird.",
		"startpageTitle": "Die Überschrift, die auf der Startseite angezeigt wird."
}
}```
